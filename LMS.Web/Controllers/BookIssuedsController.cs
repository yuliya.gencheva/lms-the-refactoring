﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LMS.Data;
using LMS.Data.Models;
using LMS.Services.Contracts;

namespace LMS.WEB.Controllers
{
    public class BookIssuedsController : Controller
    {
        private readonly IRentalManager _rentalManager;

        public BookIssuedsController(IRentalManager rentalManager)
        {
            _rentalManager = rentalManager;
        }

        // GET: BookIssueds

        public async Task<IActionResult> Index(string userId)
        {
            var listOfBookIssued = await _rentalManager.AllBooksIssued(userId);
            return View(listOfBookIssued);
        }

        // GET: BookIssueds/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookIssued = await _rentalManager.GetBookIssued(id);

            if (bookIssued == null)
            {
                return NotFound();
            }

            return View(bookIssued);
        }

        // GET: BookIssueds/Create
        //public IActionResult Create()
        //{
        //    ViewData["BookItemId"] = new SelectList(_context.BookItems, "BookItemId", "BookItemId");
        //    ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id");
        //    return View();
        //}

        // POST: BookIssueds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BookIssuedId,DateOfIssue,UserId,BookItemId,IsDeleted")] BookIssued bookIssued)
        {
            //if (ModelState.IsValid)
            //{
            //    _context.Add(bookIssued);
            //    await _context.SaveChangesAsync();
            //    return RedirectToAction(nameof(Index));
            //}

            return View(/*bookIssued*/);
        }
    }
}



// GET: BookIssueds/Edit/5
//public async Task<IActionResult> Edit(int? id)
//{
//    if (id == null)
//    {
//        return NotFound();
//    }

//    var bookIssued = await _context.BooksIssued.FindAsync(id);
//    if (bookIssued == null)
//    {
//        return NotFound();
//    }
//    ViewData["BookItemId"] = new SelectList(_context.BookItems, "BookItemId", "BookItemId", bookIssued.BookItemId);
//    ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", bookIssued.UserId);
//    return View(bookIssued);
//}

// POST: BookIssueds/Edit/5
// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//[HttpPost]
//[ValidateAntiForgeryToken]
//public async Task<IActionResult> Edit(int id, [Bind("BookIssuedId,DateOfIssue,UserId,BookItemId,IsDeleted")] BookIssued bookIssued)
//{
//    if (id != bookIssued.BookIssuedId)
//    {
//        return NotFound();
//    }

//    if (ModelState.IsValid)
//    {
//        try
//        {
//            _context.Update(bookIssued);
//            await _context.SaveChangesAsync();
//        }
//        catch (DbUpdateConcurrencyException)
//        {
//            if (!BookIssuedExists(bookIssued.BookIssuedId))
//            {
//                return NotFound();
//            }
//            else
//            {
//                throw;
//            }
//        }
//        return RedirectToAction(nameof(Index));
//    }
//    ViewData["BookItemId"] = new SelectList(_context.BookItems, "BookItemId", "BookItemId", bookIssued.BookItemId);
//    ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", bookIssued.UserId);
//    return View(bookIssued);
//}

// GET: BookIssueds/Delete/5
//public async Task<IActionResult> Delete(int? id)
//{
//    if (id == null)
//    {
//        return NotFound();
//    }

//    var bookIssued = await _context.BooksIssued
//        .Include(b => b.BookItem)
//        .Include(b => b.User)
//        .FirstOrDefaultAsync(m => m.BookIssuedId == id);
//    if (bookIssued == null)
//    {
//        return NotFound();
//    }

//    return View(bookIssued);
//}

// POST: BookIssueds/Delete/5
//[HttpPost, ActionName("Delete")]
//[ValidateAntiForgeryToken]
//public async Task<IActionResult> DeleteConfirmed(int id)
//{
//    var bookIssued = await _context.BooksIssued.FindAsync(id);
//    _context.BooksIssued.Remove(bookIssued);
//    await _context.SaveChangesAsync();
//    return RedirectToAction(nameof(Index));
//}

//private bool BookIssuedExists(int id)
//{
//    return _context.BooksIssued.Any(e => e.BookIssuedId == id);
//}
