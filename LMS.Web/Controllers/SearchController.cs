﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Services.Contracts;
using LMS.WEB.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace LMS.WEB.Controllers
{
    public class SearchController : Controller
    {
        IBookManager _bookManager;
        public SearchController(IBookManager bookManager)
        {
            _bookManager = bookManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route(nameof(SearchResults))]
        public IActionResult SearchResults(string searchBy, string criteria)
        {
            var book = this._bookManager.GetBooksByCriteria(searchBy, criteria);
            var libraryView = new SearchResultViewModel(book);
            return View(libraryView);
        }
    }
}