﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LMS.Data;
using LMS.Data.Models;
using LMS.Services.Contracts;
using LMS.WEB.ViewModels;

namespace LMS.WEB.Controllers
{
    public class BookItemsController : Controller
    {
        private readonly LMSContext _context;
        private IBookItemManager _bookItemManager;
        private IBookManager _bookManager;

        public BookItemsController(LMSContext context, IBookItemManager bookItemManager, IBookManager bookManager)
        {
            _context = context;
            _bookItemManager = bookItemManager;
            _bookManager = bookManager;
        }

        // GET: BookItems
        public async Task<IActionResult> Index()
        {
            var lMSContext = _context.BookItems.Include(b => b.Book);
            return View(await lMSContext.ToListAsync());
        }

        // GET: BookItems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookItem = await _context.BookItems
                .Include(b => b.Book)
                .FirstOrDefaultAsync(m => m.BookItemId == id);
            if (bookItem == null)
            {
                return NotFound();
            }

            return View(bookItem);
        }
        [HttpGet]
        public IActionResult Create(int? id)
        {
            var bookItemViewModel = new BookItemViewModel() { BookId = id };
            return View(bookItemViewModel);
        }
        // GET: BookItems/Create
        [HttpPost]
        public async Task<IActionResult> Creates(int rackNumber, int? bookId)
        {
            await this._bookItemManager.AddBookItem(bookId, rackNumber);
            return Redirect(nameof(Index));
        }

        // POST: BookItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("BookItemId,ReferenceOnly,BookStatus,RackNumber,BookID,IsDeleted")] BookItem bookItem)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(bookItem);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["BookID"] = new SelectList(_context.Books, "BookId", "Title", bookItem.BookID);
        //    return View(bookItem);
        //}

        // GET: BookItems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookItem = await _context.BookItems.FindAsync(id);
            if (bookItem == null)
            {
                return NotFound();
            }
            ViewData["BookID"] = new SelectList(_context.Books, "BookId", "Title", bookItem.BookID);
            return View(bookItem);
        }

        // POST: BookItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BookItemId,ReferenceOnly,BookStatus,RackNumber,BookID,IsDeleted")] BookItem bookItem)
        {
            if (id != bookItem.BookItemId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bookItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookItemExists(bookItem.BookItemId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookID"] = new SelectList(_context.Books, "BookId", "Title", bookItem.BookID);
            return View(bookItem);
        }

        // GET: BookItems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bookItem = await _context.BookItems
                .Include(b => b.Book)
                .FirstOrDefaultAsync(m => m.BookItemId == id);
            if (bookItem == null)
            {
                return NotFound();
            }

            return View(bookItem);
        }

        // POST: BookItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bookItem = await _context.BookItems.FindAsync(id);
            _context.BookItems.Remove(bookItem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookItemExists(int id)
        {
            return _context.BookItems.Any(e => e.BookItemId == id);
        }
    }
}
