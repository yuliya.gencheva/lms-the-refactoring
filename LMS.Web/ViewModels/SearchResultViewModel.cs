﻿using LMS.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.WEB.ViewModels
{
    public class SearchResultViewModel
    {
        public List<BookViewModel> Books { get; set; }

        public SearchResultViewModel(IEnumerable<Book> books)
        {
            this.Books = new List<BookViewModel>();
            foreach (var book in books)
            {
                this.Books.Add(new BookViewModel(book));
            }
        }
    }
}
