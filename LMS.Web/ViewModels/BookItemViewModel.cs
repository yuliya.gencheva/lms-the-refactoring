﻿using LMS.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.WEB.ViewModels
{
    public class BookItemViewModel
    {

        public int RackNumber { get; set; }
        public int? BookId { get; set; }
    }
}
