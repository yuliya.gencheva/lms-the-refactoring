﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Core.Commands.ModifyBooks
{
    public class RemoveBookCommand : ICommand
    {
        private readonly IBookManager bookManager;

        public RemoveBookCommand(IBookManager bookManager)
        {
            this.bookManager = bookManager;
        }

        public string Execute(IList<string> args)
        {
            string title;
            string author;
            int year;

            try
            {
                title = args[0].ToLower();
                author = args[1].ToLower();
                year = int.Parse(args[2]);
            }
            catch
            {
                throw new ArgumentException("Cannot parse book parameters!");
            }

            bookManager.RemoveBook(title, author, year);

            return $"The book {title} by {author} published in {year} was successfully removed!";

        }
    }
}