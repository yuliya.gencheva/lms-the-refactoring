﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Core.Commands.ModifyBooks
{
    public class AddBookItemCommand : ICommand
    {
        private readonly IBookItemManager bookItemManager;

        public AddBookItemCommand(IBookItemManager bookItemManager)
        {
            this.bookItemManager = bookItemManager;
        }

        public string Execute(IList<string> args)
        {
            string title;
            string author;
            int year;
            int rack;

            try
            {
                title = args[0].ToLower();
                author = args[1].ToLower();
                year = int.Parse(args[2]);
                rack = int.Parse(args[3]);
            }
            catch
            {
                throw new ArgumentException("Cannot Parse bookitem parametres!");
            }

            bookItemManager.AddBookItem(title, author, year, rack);

            return $"New copy of the book {title} published in {year} was successfully added to the library register.";
        }
    }
}
