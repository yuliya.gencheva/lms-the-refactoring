﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.ModifyBooks
{
    public class ModifyBookItemCommand : ICommand
    {
        private readonly IBookItemManager bookItemManager;

        public ModifyBookItemCommand(IBookItemManager bookItemManager)
        {
            this.bookItemManager = bookItemManager;

        }
        public string Execute(IList<string> args)
        {

            //librarian can modify referenceonly/ rackNumber or bookstatus

            int id = int.Parse(args[0]);
            string criteria = args[1];
            string newParameter = args[2];
            bookItemManager.ModifyBookItem(id, criteria, newParameter);

            return $"BookItem with id {id} succesfully modified!";
        }
    }
}
