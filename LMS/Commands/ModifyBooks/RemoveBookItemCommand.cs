﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Core.Commands.ModifyBooks
{
    public class RemoveBookItemCommand : ICommand
    {
        private readonly IBookItemManager bookItemManager;

        public RemoveBookItemCommand(
            IBookItemManager bookItemManager)
        {
            this.bookItemManager = bookItemManager;
        }

        public string Execute(IList<string> args)
        {
            int bookItemId;
            try
            {
                bookItemId = int.Parse(args[0]);
            }
            catch
            {
                throw new ArgumentException("Can't parse bookItem id!");
            }

            bookItemManager.RemoveBookItem(bookItemId);

            return $"Bookitem with id:{bookItemId} was successfully removed!";
        }
    }
}
