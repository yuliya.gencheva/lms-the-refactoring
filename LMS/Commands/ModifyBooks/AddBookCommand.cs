﻿using LMS.Core.Contracts;
using LMS.Data.Common;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Core.Commands.ModifyBooks
{
    class AddBookCommand : ICommand
    {
        private readonly IBookManager bookManager;
        public AddBookCommand(IBookManager bookManager)
        {
            this.bookManager = bookManager;
        }

        public string Execute(IList<string> args)
        {
            string title;
            string author;
            int pages;
            int year;
            string country;
            string language;
            string link;
            string isbn;
            CategoryType category;

            try
            {
                title = args[0];
                author = args[1];
                pages = int.Parse(args[2]);
                year = int.Parse(args[3]);
                country = args[4];
                language = args[5];
                link = args[6];
                isbn = args[7];
                category = (CategoryType)Enum.Parse(typeof(CategoryType), args[8], true);
            }
            catch
            {
                throw new ArgumentException("Cannot Parse book parametres!");
            }

            bookManager.AddBook(title, author, pages, year, country, language, link, isbn, category);

            return
                $"The book {title} published in {year} was successfully added to the library catalogue."
                + Environment.NewLine
                + $"Please add a copy to the book items catalog";
        }
    }
}
