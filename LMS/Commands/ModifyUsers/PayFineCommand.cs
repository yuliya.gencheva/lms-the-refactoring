﻿using LMS.Core.Contracts;
using LMS.Data.Models;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Core.Commands.ModifyUsers
{
    public class PayFineCommand : ICommand
    {
        private ISessionManager sessionManager;
        private IUserManager userManager;
        private IFineManager fineManager;

        public PayFineCommand(
            ISessionManager sessionManager,
            IUserManager userManager,
            IFineManager fineManager)
        {
            this.sessionManager = sessionManager;
            this.userManager = userManager;
            this.fineManager = fineManager;
        }
        public string Execute(IList<string> args)
        {
            User user = userManager.GetUser(sessionManager.LoggedUser.LibraryCardUserName.ToLower());
            this.fineManager.ClearFineAmount(user);
            return "All fines successfully paid!";
        }
    }
}
