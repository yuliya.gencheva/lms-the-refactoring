﻿using LMS.Core.Contracts;
using LMS.Data.Common;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Core.Commands.ModifyUsers
{
    public class RegisterNewUserCommand : ICommand
    {
        private readonly IUserManager userManager;

        public RegisterNewUserCommand(IUserManager userManager)
        {
            userManager.ValidateIfNull("User Manager Cannot Be Null");
            this.userManager = userManager;
        }

        public string Execute(IList<string> args)
        {
            string name;
            string userName;
            int roleId;

            try
            {
                name = args[0];
                userName = args[1];
                roleId = int.Parse(args[2]);
            }
            catch
            {
                throw new ArgumentException("Cannot Parse user parametres!");
            }

            userManager.AddNewUser(name, userName, roleId);
            return $"User {userName} successfully registered!";
        }
    }
}
