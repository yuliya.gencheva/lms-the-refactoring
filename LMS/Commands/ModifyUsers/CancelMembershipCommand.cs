﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.ModifyUsers
{
    public class CancelMembershipCommand : ICommand
    {
        private readonly IUserManager userManager;
        private readonly ISessionManager sessionManager;

        public CancelMembershipCommand(
            IUserManager userManager,
            ISessionManager sessionManager)
        {
            this.userManager = userManager;
            this.sessionManager = sessionManager;
        }

        public string Execute(IList<string> args)
        {

            string userNameToDelete = args[0].ToLower();

            userManager.RemoveUser(userNameToDelete);
            if (sessionManager.LoggedUser.LibraryCardUserName == userNameToDelete)
            {
                sessionManager.LogOut();
            }

            return $"user: {userNameToDelete} has been successfully deleted";
        }
    }
}
