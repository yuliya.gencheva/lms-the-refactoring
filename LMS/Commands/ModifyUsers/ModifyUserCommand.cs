﻿using LMS.Core.Contracts;
using LMS.Data.Common;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.ModifyUsers
{
    public class ModifyUserCommand : ICommand
    {
        private readonly IUserManager userManager;
        private readonly ISessionManager sessionManager;

        public ModifyUserCommand(
            IUserManager userManager,
            ISessionManager sessionManager)
        {
            userManager.ValidateIfNull("User Manager Cannot Be Null");
            sessionManager.ValidateIfNull("Session Manager Cannot Be Null");
            this.userManager = userManager;
            this.sessionManager = sessionManager;
        }

        public string Execute(IList<string> args)
        {
            string newName = args[0];
            userManager.ModifyUser(sessionManager.LoggedUser.LibraryCardUserName, newName);
            return $"The name of user: {sessionManager.LoggedUser.LibraryCardUserName} has been changed to {args[0]}!";
        }
    }
}
