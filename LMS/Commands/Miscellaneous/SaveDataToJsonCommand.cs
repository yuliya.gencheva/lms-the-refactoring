﻿using LMS.Core.Contracts;
using LMS.Data;
using LMS.Data.Models;
using LMS.Data.Seed.IOSeed;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Core.Commands.Miscellaneous
{
    public class SaveDataToJsonCommand : ICommand
    {
        private IWriteFileDatabase writeFileDatabase;
        private LMSContext lMSContext;

        public SaveDataToJsonCommand(IWriteFileDatabase writeFileDatabase, LMSContext lMSContext)
        {
            this.writeFileDatabase = writeFileDatabase;
            this.lMSContext = lMSContext;
        }

        public string Execute(IList<string> args)
        {
            SaveBookData();
            SaveUsersData();
            SaveBookItemsData();
            SaveBookReservationData();
            SaveBooksIssuedData();
            SaveAuthorData();

            return "Data saved to Json files";
        }

        private void SaveBookData()
        {
            const string jsonAddress = @"../../../../Models/Seed/JsonFilesSeed/Books.json";
            List<Book> listBooks = this.lMSContext.Books.Include(b=>b.Author).ToList();
            writeFileDatabase.WriteJsonFile(jsonAddress, listBooks);
        }

        private void SaveAuthorData()
        {
            const string jsonAddress = @"../../../../Models/Seed/JsonFilesSeed/Authors.json";
            List<Author> listAuthors = this.lMSContext.Authors.ToList();
            writeFileDatabase.WriteJsonFile(jsonAddress, listAuthors);
        }

        private void SaveUsersData()
        {
            const string jsonAddress = @"../../../../Models/Seed/JsonFilesSeed/Users.json";
            List<User> listUses = this.lMSContext.Users.ToList();
            writeFileDatabase.WriteJsonFile(jsonAddress, listUses);
        }

        private void SaveBooksIssuedData()
        {
            const string jsonAddress = @"../../../../Models/Seed/JsonFilesSeed/BooksIssued.json";
            List<BookIssued> listBooksIssued = this.lMSContext.BooksIssued.ToList();
            writeFileDatabase.WriteJsonFile(jsonAddress, listBooksIssued);
        }

        private void SaveBookItemsData()
        {
            const string jsonAddress = @"../../../../Models/Seed/JsonFilesSeed/BooksItems.json";
            List<BookItem> listBookItems = this.lMSContext.BookItems.ToList();
            writeFileDatabase.WriteJsonFile(jsonAddress, listBookItems);
        }

        private void SaveBookReservationData()
        {
            const string jsonAddress = @"../../../../Models/Seed/JsonFilesSeed/BookReservations.json";
            List<BookReservation> listBookItems = this.lMSContext.BookReservations.ToList();
            writeFileDatabase.WriteJsonFile(jsonAddress, listBookItems);
        }
    }
}
