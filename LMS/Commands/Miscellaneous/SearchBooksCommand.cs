﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.Miscellaneous
{
    public class SearchBooksCommand : ICommand
    {
        private readonly IBookItemManager bookItemManager;

        public SearchBooksCommand(IBookItemManager bookItemManager)
        {
            this.bookItemManager = bookItemManager;
        }

        public string Execute(IList<string> args)
        {

            string searchOption = args[0].ToLower();
            string searchArgs = args[1].ToLower();

            string result = bookItemManager.GetBooksByCriteria(searchOption, searchArgs);

            return result;
        }
    }
}