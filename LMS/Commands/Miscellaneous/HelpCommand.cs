﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Core.Commands.Miscellaneous
{
    public class HelpCommand : ICommand
    {
        private readonly ISessionManager sessionManager;
        public HelpCommand(ISessionManager sessionManager)
        {
            this.sessionManager = sessionManager;
        }

        public string Execute(IList<string> args)
        {
            string str;
            switch (sessionManager.LoggedUser.RoleId)
            {
                case 1:
                    str = @"
             searchbooks/[title/author/category/year]/[parameters] - search books by given title, author,
             category or year
             checkoutbook/[bookItemId]/[UserName] - check out book for specified user
             renewbook/[book ID]/[UserName] - renew book  for specified user
             reservebook/[book id ]/[UserName] - make a book reservation for specified user
             returnbook/[book id]/[UserName] - returning book for specified user
             login/[UserName] login with user name 
             logout - logout from the current user name
             registernewuser/[name]/[UserName]/[RoleId] - registration of a new member; RoleId - 1-librarian,2-member
             addbook/[title]/[author]/[pages]/[year]/[country]/[language]/[link]/[isbn]/[category] 
             - adding book
             removebook/[title]/[author]/[year] - remove book
             addbookitem/[title]/[author]/[year]/[rack] - add book item
             removebookitem/[bookItemId] - remove book item 
             modifybookitem/[bookItemId]/[racknumber/referenceonly/bookStatus]/[Parameters]- modify book item by rack number, as referenceonly or bookStatus
             modifyuser/[newName]- modify name
             cancelmembership/[userName] - cancle membership by givem user name
             checkforoverduebooks - checking for overdue books
             libraryinfo - shows information about the library
             viewaccount - shows current status of the user
             savedatatojson - saves all table data to the relevant json file
             ";
                    return str;

                case 2:
                    str = @"
                        login/[UserName] login with user name 
                        logout - logout from the current user name
                        searchbooks/[title/author/category/year]/[parameters] - search books by given title, author, 
                        category or year
                        checkoutbook/[bookItemId] - check out book 
                        renewbook/[book ID]/[UserName] - renew book
                        reservebook/[book id ] - make a book reservation 
                        returnbook/[book id] - returning book
                        modifyuser/[newName] - modify name
                        cancelmembership/[userName] - cancel membership by given user name
                        payfine - clears the fines for the current user
                        libraryinfo - shows information about the library
                        viewaccount - shows current status of the user
                        ";
                    return str;

                default:
                    throw new ArgumentException("Invalid user role!");
            }
        }
    }
}
