﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.Miscellaneous
{
    class LibraryInfoCommand : ICommand
    {
        private readonly ISessionManager sessionManager;

        public LibraryInfoCommand(ISessionManager sessionManager)
        {
            this.sessionManager = sessionManager;
        }
        public string Execute(IList<string> args)
        {
            return this.sessionManager.ToString();
        }
    }
}
