﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.Miscellaneous
{
    class ViewAccountCommand : ICommand
    {
        private readonly ISessionManager sessionManager;

        public ViewAccountCommand(ISessionManager sessionManager)
        {
            this.sessionManager = sessionManager;
        }
        public string Execute(IList<string> args)
        {
            return sessionManager.LoggedUser.ToString();
        }
    }
}
