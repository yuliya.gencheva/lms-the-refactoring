﻿using LMS.Core.Contracts;
using LMS.Data.Models;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Core.Commands.Miscellaneous
{
    public class LoginCommand : ICommand
    {
        private readonly ISessionManager sessionManager;
        private readonly IUserManager userManager;
        public LoginCommand(ISessionManager sessionManager, IUserManager userManager)
        {
            this.sessionManager = sessionManager;
            this.userManager = userManager;
        }
        public string Execute(IList<string> args)
        {
            //TODO DELETE?
            string libraryCardUserName = args[0].ToLower();
            User user = userManager.GetUser(libraryCardUserName);
            sessionManager.LogIn(user.Name);

            //string notifications = string.Join("***", user.Notifications);

            //if (string.IsNullOrEmpty(notifications))
            //{
            //    notifications = "No new notifications";
            //}
            return $"Hello, {user.Name}!{Environment.NewLine}For a list of all available commands, write: help";
        }
    }
}
