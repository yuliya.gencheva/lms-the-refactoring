﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.Miscellaneous
{
    class LogoutCommand : ICommand
    {
        private readonly ISessionManager sessionManager;
        public LogoutCommand(ISessionManager sessionManager)
        {
            this.sessionManager = sessionManager;
        }

        public string Execute(IList<string> args)
        {
            //TODO DELETE?
            sessionManager.LogOut();
            return $"You are now logged out!";
        }
    }
}
