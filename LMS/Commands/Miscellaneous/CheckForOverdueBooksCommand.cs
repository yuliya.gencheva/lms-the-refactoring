﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.Miscellaneous
{
    public class CheckForOverdueBooksCommand : ICommand
    {
        private readonly ISessionManager sessionManager;
        public CheckForOverdueBooksCommand(
            ISessionManager sessionManager)
        {
           
            this.sessionManager = sessionManager;
        }

        public string Execute(IList<string> args)
        {

            //int sentNotifications = this.library.CheckForOverdueBooks();

            return $"No notifications for overdue books have been sent!";
        }
    }
}
