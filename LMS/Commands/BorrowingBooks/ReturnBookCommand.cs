﻿using LMS.Core.Contracts;
using LMS.Data.Common;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.BorrowingBooks
{
    class ReturnBookCommand : ICommand
    {

        private readonly IRentalManager rentalManager;


        public ReturnBookCommand(IRentalManager rentalManager)
        {
            rentalManager.ValidateIfNull("Rental Manager Cannot Be Null");
            this.rentalManager = rentalManager;
        }

        public string Execute(IList<string> args)
        {
            int bookItemId = int.Parse(args[0]);
            string userName = args[1];
            this.rentalManager.ReturnBook(bookItemId, userName);
            return $"Book with id: {bookItemId} is returned";
        }
    }
}

//TODO fines
//int daysOverdue = member.CheckForOverdueDays(bookToReturn);
//string message = "";
//if (daysOverdue > 0)
//{
//    member.ChargeFines(daysOverdue * 1.5m);
//    this.library.CollectFines(daysOverdue * 1.5m);
//    message += $"You are {daysOverdue} days to return the book. Fine has been collected!{Environment.NewLine}";
//}
//bool returned = member.ReturnBook(bookToReturn);
//if (returned)
//{
//    message += $"Book with id: {bookItemId} is returned";
//}