﻿using LMS.Core.Contracts;
using LMS.Data.Common;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.BorrowingBooks
{
    public class RenewBookCommand : ICommand
    {
        private readonly IRentalManager rentalManager;


        public RenewBookCommand(IRentalManager rentalManager)
        {
            rentalManager.ValidateIfNull("Rental Manager Cannot Be Null");
            this.rentalManager = rentalManager;
        }
        public string Execute(IList<string> args)
        {
            //TODO try catch
            int bookItemId = int.Parse(args[0]);
            string userName = args[1];

            this.rentalManager.RenewBook(bookItemId, userName);
            return $"Successfully renewed book with id {bookItemId}";

        }
    }
}















//TODO FINES!!!!
//int daysOverdue = member.CheckForOverdueDays(bookToRenew);

//string message = "";
//if (daysOverdue > 0)
//{
//    user.ChargeFines(daysOverdue * 1.5m);
//    this.library.CollectFines(daysOverdue * 1.5m);
//    message += $"You are {daysOverdue} days late to return the book. Fine has been collected!{Environment.NewLine}";
//}