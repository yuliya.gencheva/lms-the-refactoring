﻿using LMS.Core.Contracts;
using LMS.Data.Common;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.BorrowingBooks
{
    class ReserveBookCommand : ICommand
    {
        private readonly IReservationManager reservationManager;
        public ReserveBookCommand(IReservationManager reservationManager)
        {
            reservationManager.ValidateIfNull("Rental Manager Cannot Be Null");
            this.reservationManager = reservationManager;
        }
        public string Execute(IList<string> args)
        {
            int bookItemId = int.Parse(args[0]);
            string userName = args[1];
            this.reservationManager.ReserveBook(bookItemId, userName);

            return $"The book with ID: {bookItemId} is successfully reserved for user {userName}!";
        }
    }
}
