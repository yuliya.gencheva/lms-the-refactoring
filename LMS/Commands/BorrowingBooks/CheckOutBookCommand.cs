﻿using LMS.Core.Contracts;
using LMS.Data.Common;
using LMS.Services.Contracts;
using System.Collections.Generic;

namespace LMS.Core.Commands.BorrowingBooks
{
    public class CheckOutBookCommand : ICommand
    {

        private readonly IRentalManager rentalManager;


        public CheckOutBookCommand(IRentalManager rentalManager)
        {
            rentalManager.ValidateIfNull("Rental Manager Cannot Be Null");
            this.rentalManager = rentalManager;
        }

        public string Execute(IList<string> args)
        {

            int bookItemId = int.Parse(args[0]);
            string userName = args[1].ToLower();
            this.rentalManager.CheckOutBook(bookItemId, userName);

            return $"{userName} successfully checked out book: ID {bookItemId}";
        }
    }
}
