﻿namespace LMS.Core.Contracts
{
    public interface IReader
    {
        //#Abstraction
        string Read();
    }
}
