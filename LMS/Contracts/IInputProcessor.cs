﻿using System.Collections.Generic;

namespace LMS.Core.Contracts
{
    public interface IInputProcessor
    {
        (string commandName, IList<string> args) Process(string input);


    }
}