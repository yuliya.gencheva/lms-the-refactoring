﻿namespace LMS.Core.Contracts
{
    public interface IEngine
    {
        void Run();
    }
}
