﻿namespace LMS.Core.Contracts
{
    public interface IWriter
    {
        void Write(string result);
        void WriteWelcomeScreen();
    }
}
