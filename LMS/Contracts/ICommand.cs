﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Core.Contracts
{
    public interface ICommand
    {

        string Execute(IList<string> args);
    }
}
