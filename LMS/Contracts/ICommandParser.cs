﻿using System.Collections.Generic;

namespace LMS.Core.Contracts
{
    public interface ICommandParser
    {
        string ParseCommand(string commandName, IList<string> args);
    }
}