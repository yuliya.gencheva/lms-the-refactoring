﻿using Autofac;
using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System;

namespace LMS.Core.Infrastructure.Engines
{
    public class BaseEngine : IEngine
    {
        private readonly ISessionManager sessionManager;
        private readonly IReader reader;
        private readonly IWriter writer;
        private readonly IComponentContext componentCommand;

        public BaseEngine(
            ISessionManager sessionManager, IReader reader,
            IWriter writer, IComponentContext componentCommand)
        {
            this.sessionManager = sessionManager;
            this.reader = reader;
            this.writer = writer;
            this.componentCommand = componentCommand;
        }

        public void Run()
        {
            writer.WriteWelcomeScreen();
            writer.Write("Please enter your username:");
            string input;
            IEngine engine;
            while ((input = reader.Read()) != "end")
            {
                try
                {
                    if (string.IsNullOrEmpty(input) || input == "guest")
                    {
                        engine = componentCommand.ResolveNamed<IEngine>("guest");
                    }
                    else
                    {
                        writer.Write(sessionManager.LogIn(input));
                        if (sessionManager.LoggedUser.RoleId == 1)
                        {
                            engine = componentCommand.ResolveNamed<IEngine>("librarian");
                        }
                        else
                        {
                            engine = componentCommand.ResolveNamed<IEngine>("member");
                        }

                    }
                    engine.Run();
                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                    }

                    writer.Write($"ERROR: {ex.Message}" + Environment.NewLine + "***************");
                }

                if (sessionManager.LoggedUser != null)
                {
                    writer.Write(sessionManager.LogOut());
                }
                writer.Write("Please enter your username or type end to exit the system!");
            }
        }
    }
}
