﻿using LMS.Core.Contracts;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace LMS.Core.Infrastructure.Engines
{
    class MemberEngine : IEngine
    {
        private readonly ISessionManager sessionManager;
        private readonly ICommandParser commandParser;
        private readonly IReader reader;
        private readonly IWriter writer;
        private readonly IInputProcessor inputProcessor;
        private ReadOnlyCollection<string> permittedCommands = new ReadOnlyCollection<string>(new List<string>

        { "help", "searchbooks", "checkoutbook", "renewbook", "reservebook", "returnbook", "modifyuser",
                "cancelmembership", "libraryinfo", "viewaccount", "payfine" });

        public MemberEngine(
             ISessionManager sessionManager,
            ICommandParser commandParser, IReader reader,
            IWriter writer, IInputProcessor inputProcessor)
        {
            this.sessionManager = sessionManager;
            this.commandParser = commandParser;
            this.reader = reader;
            this.writer = writer;
            this.inputProcessor = inputProcessor;
        }
        public void Run()
        {
            string consoleInput;
            string result;
            writer.Write("Enter command:");
            while (sessionManager.LoggedUser != null && (consoleInput = reader.Read()) != "logout")
            {
                var (commandName, args) = inputProcessor.Process(consoleInput);

                if (permittedCommands.Contains(commandName))
                {
                    if (commandName == "checkoutbook" || commandName == "renewbook" || commandName == "reservebook" ||
                        commandName == "returnbook" || commandName == "cancelmembership")
                    {
                        args.Add(sessionManager.LoggedUser.LibraryCardUserName);
                    }
                    try
                    {
                        result = commandParser.ParseCommand(commandName, args);
                        writer.Write(result + Environment.NewLine + "***************");
                    }
                    catch (Exception ex)
                    {
                        while (ex.InnerException != null)
                        {
                            ex = ex.InnerException;
                        }

                        writer.Write($"ERROR: {ex.Message}" + Environment.NewLine + "***************");
                    }


                }
                else
                {
                    writer.Write("Invalid command! Write help for a list of available commands.");
                    continue;
                }
            }
        }
    }
}
