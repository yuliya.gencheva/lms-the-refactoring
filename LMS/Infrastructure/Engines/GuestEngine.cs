﻿using LMS.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Core.Infrastructure.Engines
{
    public class GuestEngine : IEngine
    {
        private readonly ICommandParser commandParser;
        private readonly IReader reader;
        private readonly IWriter writer;
        private readonly IInputProcessor inputProcessor;

        public GuestEngine(
            ICommandParser commandParser,
            IReader reader, IWriter writer,
            IInputProcessor inputProcessor)
        {
            this.commandParser = commandParser;
            this.reader = reader;
            this.writer = writer;
            this.inputProcessor = inputProcessor;
        }
        public void Run()
        {
            string result;
            string consoleInput;

            writer.Write("Welcome, Guest!");
            writer.Write($"You can search for books:  searchbooks [title/author/category/year] [parameters]"
                + Environment.NewLine
                + $"Or see the library information: libraryinfo");

            while (!string.IsNullOrEmpty(consoleInput = reader.Read()))
            {
                var (commandName, args) = inputProcessor.Process(consoleInput);
                if (commandName != "libraryinfo" && commandName != "searchbooks")
                {
                    writer.Write("Invalid command!");
                    continue;
                }

                try
                {
                    result = commandParser.ParseCommand(commandName, args);
                    writer.Write(result + Environment.NewLine + "***************");
                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                    }

                    writer.Write($"ERROR: {ex.Message}" + Environment.NewLine + "***************");
                }
            }
        }
    }
}
