﻿using LMS.Core.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Core.Infrastructure
{
    class CommandParser : ICommandParser
    {
        private readonly ICommandFactory commandFactory;

        public CommandParser(ICommandFactory commandFactory)
        {
            this.commandFactory = commandFactory;
        }

        public string ParseCommand(string commandName, IList<string> args)
        {


            ICommand command;
            try
            {
                command = commandFactory.CreateCommand(commandName);

            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid command name!");
            }
            return command.Execute(args);

        }
    }
}
