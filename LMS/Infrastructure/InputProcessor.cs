﻿using LMS.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Core.Infrastructure
{
    public class InputProcessor : IInputProcessor
    {

        public (string commandName, IList<string> args) Process(string input)
        {
            var splitInput = input.Split('/');
            return (splitInput[0], splitInput.Skip(1).ToList());

        }
    }
}
