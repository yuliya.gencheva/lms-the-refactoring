﻿using LMS.Core.Contracts;
using System;

namespace LMS.Core.Infrastructure
{
    class ConsoleReader : IReader
    {
        public string Read()

        {
            return Console.ReadLine();
        }
    }
}
