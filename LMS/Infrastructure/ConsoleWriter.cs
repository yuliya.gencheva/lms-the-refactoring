﻿using LMS.Core.Contracts;
using System.Drawing;
using Console = Colorful.Console;

namespace LMS.Core.Infrastructure
{
    public class ConsoleWriter : IWriter
    {
        public void Write(string result)
        {
            Console.WriteLine(result);
        }

        public void WriteWelcomeScreen()
        {
            int DA = 34;
            int V = 139;
            int ID = 34;
            string msg = "JULIA & JORDAN";
            Console.WriteAscii(msg, Color.FromArgb(DA, V, ID));
            Console.WriteLine(@"##################################
##################################
###	   Welcome To          ###
###   	   Trinity College     ###
###	   Dublin Library      ###
##################################
##################################" + "\r\n\r\n");
        }
    }
}
