﻿using Autofac;
using System;
using System.Reflection;
using System.Linq;
using LMS.Data;
using LMS.Data.Seed.IOSeed;
using LMS.Core.Contracts;
using LMS.Core.Factories;
using LMS.Core.Infrastructure.Engines;
using LMS.Core.Infrastructure;
using LMS.Services.Contracts;
using LMS.Services.Factories;
using LMS.Services;
using Microsoft.EntityFrameworkCore;

namespace LMS.Core
{
    public class ContainerConfig
    {
        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<BaseEngine>().Named<IEngine>("base");
            builder.RegisterType<GuestEngine>().Named<IEngine>("guest");
            builder.RegisterType<MemberEngine>().Named<IEngine>("member");
            builder.RegisterType<LibararianEngine>().Named<IEngine>("librarian");
            builder.RegisterType<CommandParser>().As<ICommandParser>();
            builder.RegisterType<InputProcessor>().As<IInputProcessor>();
            builder.RegisterType<LMSContext>().AsSelf().InstancePerLifetimeScope();
            //builder.RegisterType<Configurator>().AsSelf().InstancePerLifetimeScope();
            RegisterIOProviders(builder);
            //RegisterDatabases(builder);
            RegisterServices(builder);
            RegisterFactories(builder);
            RegisterCommands(builder);

            return builder.Build();
        }

        private static void RegisterCommands(ContainerBuilder builder)
        {
            var commands = Assembly
                            .GetExecutingAssembly()
                            .DefinedTypes
                            .Where(t => t.ImplementedInterfaces.Contains(typeof(ICommand)))
                            .ToList();

            foreach (var command in commands)
            {
                builder.RegisterType(command.AsType()).Named<ICommand>(command.Name.ToLower());
            }
        }

        private static void RegisterFactories(ContainerBuilder builder)
        {
            builder.RegisterType<BookFactory>().As<IBookFactory>();
            builder.RegisterType<BookItemFactory>().As<IBookItemFactory>();
            builder.RegisterType<BookIssuedFactory>().As<IBookIssuedFactory>();
            builder.RegisterType<BookReservationFactory>().As<IBookReservationFactory>();
            builder.RegisterType<CommandFactory>().As<ICommandFactory>();
            builder.RegisterType<UserFactory>().As<IUserFactory>();
        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<BookitemManager>().As<IBookItemManager>().SingleInstance();
            builder.RegisterType<BookManager>().As<IBookManager>().SingleInstance();
            builder.RegisterType<UserManager>().As<IUserManager>().SingleInstance();
            builder.RegisterType<SessionManager>().As<ISessionManager>().SingleInstance();
            builder.RegisterType<FineManager>().As<IFineManager>().SingleInstance();
            builder.RegisterType<ReservationManager>().As<IReservationManager>().SingleInstance();
            builder.RegisterType<RentalManager>().As<IRentalManager>().SingleInstance();
        }


        private static void RegisterIOProviders(ContainerBuilder builder)
        {
            builder.RegisterType<ReadFileDatabase>().AsSelf();
            builder.RegisterType<WriteFileDatabase>().As<IWriteFileDatabase>();
            builder.RegisterType<ConsoleReader>().As<IReader>();
            builder.RegisterType<ConsoleWriter>().As<IWriter>();
        }
    }
}

        //private static void RegisterDatabases(ContainerBuilder builder)
        //{
        //    builder.RegisterType<BookReservationDatabase>().As<IBookReservationDatabase>().SingleInstance();
        //    builder.RegisterType<BookIssuedDatabase>().As<IBookIssuedDatabase>().SingleInstance();
        //    builder.RegisterType<BookItemDatabase>().As<IBookItemDatabase>().SingleInstance();
        //    builder.RegisterType<BooksDatabase>().As<IBookDatabase>().SingleInstance();
        //    builder.RegisterType<UserDatabase>().As<IUserDatabase>().SingleInstance();
        //}