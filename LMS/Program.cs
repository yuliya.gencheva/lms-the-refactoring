﻿using Autofac;
using LMS.Core.Contracts;
using LMS.Data;
using LMS.Data.Seed;

namespace LMS.Core
{
    public class Program
    {
        static void Main(string[] args)
        {

            var container = new ContainerConfig().Configure();

            using (var scope = container.BeginLifetimeScope())
            {
                var context = container.Resolve<LMSContext>();
                //DataSeeder.SeedAuthors(context);
                //DataSeeder.SeedRoles(context);
                //DataSeeder.SeedUsers(context);
                //DataSeeder.SeedBooks(context);
                //DataSeeder.SeedBookItems(context);
                //DataSeeder.SeedBooksIssued(context);
                //DataSeeder.SeedBookReservations(context);
                IEngine engine = scope.ResolveNamed<IEngine>("base");
                //var user = new User("dddd", "jul", 0);
                //context.Users.Add( user);
                // var bookItem = context.BookItems.Find(8);
                //var user = context.Users.Find(5);
                //var fine = new Fine {Amount=10, Date=DateTime.Now, UserId=user.Id };
                //context.BooksIssued.Add(new BookIssued {User=user,BookItem=bookItem, DateOfIssue=DateTime.Now.AddDays(-20)});
                //context.BookReservations.Add(new BookReservation(bookItem, user));
                //context.Fines.Add(fine);
                //context.SaveChanges();

                engine.Run();
            }
        }
    }
}
