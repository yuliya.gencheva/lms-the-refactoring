﻿using Autofac;
using LMS.Core.Contracts;
using System;

namespace LMS.Core.Factories
{
    //#SingleResponsibility
    public class CommandFactory : ICommandFactory
    {
        private readonly IComponentContext componentCommand;

        public CommandFactory(IComponentContext componentCommand)
        {
            this.componentCommand = componentCommand;
        }

        public ICommand CreateCommand(string commandName)
        {
            var command = componentCommand.ResolveNamed<ICommand>(commandName.ToLower() + "command");
            return command;
        }
    }
}
