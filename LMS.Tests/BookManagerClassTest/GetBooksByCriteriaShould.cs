﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Moq;
using Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.BookManagerClassTest
{
    [TestClass]
    public class GetBooksByCriteriaShould
    {
        Mock<IBookItemDatabase> bookItemDatabaseMock;
       
        public GetBooksByCriteriaShould()
        {
            this.bookItemDatabaseMock = new Mock<IBookItemDatabase>();
        }

       
        [DataTestMethod]
        [DataRow("title", "testtitle")]
        [DataRow("author", "testauthor")]
        [DataRow("category", "detective")]
        [DataRow("year", "1950")]
        public void ReturnCorrectBookItems(string searchCriteria, string searchOption)
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            var bookItemMock2 = new Mock<IBookItem>();
            var bookItemMock3 = new Mock<IBookItem>();
            bookItemMock.SetupGet(b => b.Book.Author).Returns("TestAuthor");
            bookItemMock2.SetupGet(b => b.Book.Author).Returns("TestAuthor");
            bookItemMock3.SetupGet(b => b.Book.Author).Returns("NotTestAuthor");
            bookItemMock.SetupGet(b => b.Book.Title).Returns("TestTitle");
            bookItemMock2.SetupGet(b => b.Book.Title).Returns("TestTitle");
            bookItemMock3.SetupGet(b => b.Book.Title).Returns("NotTestTitle");
            bookItemMock.SetupGet(b => b.Book.Category).Returns(Models.Common.CategoryType.Detective);
            bookItemMock2.SetupGet(b => b.Book.Category).Returns(Models.Common.CategoryType.Detective);
            bookItemMock3.SetupGet(b => b.Book.Category).Returns(Models.Common.CategoryType.Fantasy);
            bookItemMock.SetupGet(b => b.Book.Year).Returns(1950);
            bookItemMock2.SetupGet(b => b.Book.Year).Returns(1950);
            bookItemMock3.SetupGet(b => b.Book.Year).Returns(1960);

            this.bookItemDatabaseMock
                .Setup(bdb => bdb.BookItems)
                .Returns(new List<IBookItem> { bookItemMock.Object, bookItemMock2.Object, bookItemMock3.Object });

            //Act
            var result=sut.GetBooksByCriteria(searchCriteria, searchOption).Split("***************");
            //Assert
            Assert.AreEqual(2, result.Length);
        }

        [DataTestMethod]
        [DataRow("ffffff", "test")]
        public void ThrowException_WhenInvalidCriteriaIsPassed(string searchCriteria, string searchOption)
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            //Act&Assert
            Assert.ThrowsException<ArgumentException>(() => sut.GetBooksByCriteria(searchCriteria, searchOption));
        }

        [DataTestMethod]
        [DataRow("ffffff", "test")]
        public void GetCorrectExceptionMessage_WhenInvalidCriteriaIsPassed(string searchCriteria, string searchOption)
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            //Act
            var ex = Assert.ThrowsException<ArgumentException>(() => sut.GetBooksByCriteria(searchCriteria, searchOption));
            //Assert
            Assert.AreEqual("Invalid search criteria", ex.Message);

        }


    }
}
