﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Moq;
using Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.BookManagerClassTest
{
    [TestClass]
    public class AddNewBookItemShould
    {
        Mock<IBookItemDatabase> bookItemDatabaseMock;

        public AddNewBookItemShould()
        {
            this.bookItemDatabaseMock = new Mock<IBookItemDatabase>();
        }

        [TestMethod]
        public void ThrowException_WhenPassedBookIsNull()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            //Act&Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.AddBookItem(null));
        }

        [TestMethod]
        public void ThrowException_WhenBookItemIsAlreadyInDB()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            var listBookItems = new List<IBookItem> { bookItemMock.Object };
            bookItemDatabaseMock.Setup(b => b.BookItems).Returns(listBookItems);
            //Act&Assert
            Assert.ThrowsException<ArgumentException>(() => sut.AddBookItem(bookItemMock.Object));
        }

        [TestMethod]
        public void PassBookItemToDB_WhenBookItemIsValid()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            bookItemDatabaseMock
              .Setup(b => b.Add(bookItemMock.Object)).Verifiable();
            bookItemDatabaseMock.Setup(b => b.BookItems).Returns(new List<IBookItem>());
            //Act
            sut.AddBookItem(bookItemMock.Object);
           //Assert
            bookItemDatabaseMock.Verify((b) => b.Add(bookItemMock.Object), Times.Once);
        }
    }
}
