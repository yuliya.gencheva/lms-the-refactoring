﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Moq;
using Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.BookManagerClassTest
{
    [TestClass]
    public class ModifyBookItemShould
    {
        Mock<IBookItemDatabase> bookItemDatabaseMock;

        
        public ModifyBookItemShould()
        {
            this.bookItemDatabaseMock = new Mock<IBookItemDatabase>();
        }

        [TestMethod]
        public void ThrowException_WhenPassedBookIsNull()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            //Act&Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.ModifyBookItem(null));
        }

        [TestMethod]
        public void CallModifyMethodFromDB_WhenBookItemIsValid()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            bookItemDatabaseMock
              .Setup(b => b.Modify(bookItemMock.Object)).Verifiable();
            //Act
            sut.ModifyBookItem(bookItemMock.Object);
            //Assert
            bookItemDatabaseMock.Verify((b) => b.Modify(bookItemMock.Object), Times.Once);
        }
    }
}
