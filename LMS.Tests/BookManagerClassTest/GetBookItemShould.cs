﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Moq;
using Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.BookManagerClassTest
{
    [TestClass]
    public class GetBookItemShould
    {
        Mock<IBookItemDatabase> bookItemDatabaseMock;

        public GetBookItemShould()
        {
            this.bookItemDatabaseMock = new Mock<IBookItemDatabase>();
        }

        [TestMethod]
        public void GetBookItemFromDB_WhenBookItemExists()
        {
            //Arrange
            var sut = new BookitemManager(this.bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            this.bookItemDatabaseMock
                .Setup(b => b.Get(7))
                .Returns(bookItemMock.Object);
            //Act
            sut.GetBookItem(7);
            //Assert
            this.bookItemDatabaseMock.Verify(b => b.Get(7), Times.Once);

        }

        [TestMethod]
        public void ThrowException_WhenBookItemDoesNotExist()
        {
            //Arrange
            var sut = new BookitemManager(this.bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            bookItemMock
                .Setup(b => b.Id)
                .Returns(7);
            this.bookItemDatabaseMock
                .Setup(b => b.Get(7))
                .Returns(bookItemMock.Object);
            //Act&Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.GetBookItem(8));

        }

        [TestMethod]
        public void ReturnFoundBookItem()
        {
            //Arrange
            var sut = new BookitemManager(this.bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            bookItemMock
                .Setup(b => b.Id)
                .Returns(7);
            this.bookItemDatabaseMock
                .Setup(b => b.Get(7))
                .Returns(bookItemMock.Object);
            //Act
           var result= sut.GetBookItem(7);
            //Assert
            Assert.AreEqual(bookItemMock.Object, result);

        }

    }
}
