﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Moq;
using Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.BookManagerClassTest
{
    [TestClass]
    public class GetNextAvailableIDShould
    {
        Mock<IBookItemDatabase> bookItemDatabaseMock;

        public GetNextAvailableIDShould()
        {
            this.bookItemDatabaseMock = new Mock<IBookItemDatabase>();
        }

        [TestMethod]
        public void ReturnCorrectID()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            bookItemMock.Setup(b => b.Id).Returns(5);
            this.bookItemDatabaseMock
               .Setup(bdb => bdb.BookItems)
               .Returns(new List<IBookItem> { bookItemMock.Object});
            //Act
            var result = sut.GetNextAvailableID();
            //Assert
            Assert.AreEqual(6, result);

        }
    }
}
