﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Moq;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Tests.BookManagerClassTest
{
    [TestClass]
    public class GetBookItemsFromBookShould
    {
        Mock<IBookItemDatabase> bookItemDatabaseMock;

        public GetBookItemsFromBookShould()
        {
            this.bookItemDatabaseMock = new Mock<IBookItemDatabase>();
        }

        [TestMethod]
        public void GetBookItemsFromDBforSpecificBook_WhenBookItemsExist()
        {
            //Arrange
            var sut = new BookitemManager(this.bookItemDatabaseMock.Object);
            var bookMock = new Mock<IBook>();
            var bookItemMock = new Mock<IBookItem>();
            var bookItemMock2 = new Mock<IBookItem>();
            bookItemMock.Setup(b => b.Book).Returns(bookMock.Object);
            bookItemMock2.Setup(b => b.Book).Returns(bookMock.Object);
            this.bookItemDatabaseMock
                .Setup(bdb => bdb.BookItems)
                .Returns(new List<IBookItem> { bookItemMock.Object, bookItemMock2.Object});
            //Act
            List<IBookItem> result= sut.GetBookItemsFromBook(bookMock.Object);
            //Assert
            Assert.AreEqual(2 , result.Count);

        }

        [TestMethod]
        public void ReturnEmptyList_WhenNoBookItemsExistinDB()
        {
            //Arrange
            var sut = new BookitemManager(this.bookItemDatabaseMock.Object);
            var bookMockInDB = new Mock<IBook>();
            var bookMockNotInDB = new Mock<IBook>();
            var bookItemMock = new Mock<IBookItem>();
            var bookItemMock2 = new Mock<IBookItem>();
            bookItemMock.Setup(b => b.Book).Returns(bookMockInDB.Object);
            bookItemMock2.Setup(b => b.Book).Returns(bookMockInDB.Object);
            this.bookItemDatabaseMock
                .Setup(bdb => bdb.BookItems)
                .Returns(new List<IBookItem> { bookItemMock.Object, bookItemMock2.Object });
            //Act
            List<IBookItem> result = sut.GetBookItemsFromBook(bookMockNotInDB.Object);
            //Assert
            Assert.AreEqual(0, result.Count);

        }
    }
}
