﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.BookManagerClassTest
{
    [TestClass]
    public class ConstructorShould
    {

        [TestMethod]
        public void CorrectlyInstantiateObject_WhenValidValuesPassed()
        {
            //Arrange
           var bookItemDatabaseMock = new Mock<IBookItemDatabase>();
            //Act
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            //Assert
            Assert.IsInstanceOfType(sut, typeof(IBookItemManager));
        }


    }
}
