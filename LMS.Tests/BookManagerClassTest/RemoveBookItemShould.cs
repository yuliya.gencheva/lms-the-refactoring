﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Moq;
using Services;
using System;
using System.Collections.Generic;
using System.Text;
using Models.Common;

namespace LMS.Tests.BookManagerClassTest
{
    [TestClass]
    public class RemoveBookItemShould
    {
        Mock<IBookItemDatabase> bookItemDatabaseMock;

        public RemoveBookItemShould()
        {
            this.bookItemDatabaseMock = new Mock<IBookItemDatabase>();
        }

        [TestMethod]
        public void ThrowException_WhenPassedBookIsNull()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            //Act&Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.AddBookItem(null));
        }

        [TestMethod]
        public void ThrowException_WhenBookItemIsLoaned()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            bookItemMock.Setup(b => b.BookStatus).Returns(BookStatus.Loaned);
            //Act&Assert
            Assert.ThrowsException<ArgumentException>(() => sut.RemoveBookItem(bookItemMock.Object));
        }

        [TestMethod]
        public void ThrowException_WhenBookItemIsReserved()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            bookItemMock.Setup(b => b.BookStatus).Returns(BookStatus.Reserved);
            //Act&Assert
            Assert.ThrowsException<ArgumentException>(() => sut.RemoveBookItem(bookItemMock.Object));
        }

        [TestMethod]
        public void CallRemoveMethodFromDB_WhenBookItemIsValid()
        {
            //Arrange
            var sut = new BookitemManager(bookItemDatabaseMock.Object);
            var bookItemMock = new Mock<IBookItem>();
            bookItemMock.Setup(b => b.BookStatus).Returns(BookStatus.Available);
            bookItemDatabaseMock
              .Setup(b => b.Remove(bookItemMock.Object)).Verifiable();
            //Act
            sut.RemoveBookItem(bookItemMock.Object);
            //Assert
            bookItemDatabaseMock.Verify((b) => b.Remove(bookItemMock.Object), Times.Once);
        }
    }
}
