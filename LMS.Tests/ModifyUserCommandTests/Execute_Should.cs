﻿using Core.Commands.ModifyUsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Common;
using Models.Contracts;
using Moq;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.ModifyUserCommandTests
{
    [TestClass]
    public class Execute_Should
    {
        [TestMethod]
        public void Call_ModifyUser_When_Passed_Valid_Value()
        {
            //Arrange
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.Name).Returns("Petio");
            userMock.Setup(u => u.LibraryCardUserName).Returns("jordang");
            userMock.Setup(u => u.Role).Returns(Role.Librarian);
            var userManagerMock = new Mock<IUserManager>();
            userManagerMock.Setup(uM => uM.GetUser(userMock.Object.LibraryCardUserName)).Returns(userMock.Object);
            var sessionManagerMock = new Mock<ISessionManager>();
            sessionManagerMock.Setup(u => u.LoggedUser).Returns(userMock.Object);

            var sut = new ModifyUserCommand(
                userManagerMock.Object,
                sessionManagerMock.Object);
            //Act
            sut.Execute("Dancho");
            //Assert
            userManagerMock.Verify(m => m.ModifyUser(userMock.Object), Times.Once);
        }
    }
}
