﻿using Core.Commands.ModifyUsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.ModifyUserCommandTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Correctly_Instantade_ModifyCommand_When_Valid_Values_Passed()
        {
            //Arrange
            var userManagerMock = new Mock<IUserManager>();
            var sessionManagerMock = new Mock<ISessionManager>();

            //Act
            var sut = new ModifyUserCommand(
                userManagerMock.Object,
                sessionManagerMock.Object);

            //Assert
            Assert.IsInstanceOfType(sut, typeof(ModifyUserCommand));
        }

        [TestMethod]
        public void Throws_NullException_When_Passed_Null_UserManager()
        {
            //Arrange
            var userManagerMock = new Mock<IUserManager>();
            var sessionManagerMock = new Mock<ISessionManager>();

            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(
                () => new ModifyUserCommand(
                null,
                sessionManagerMock.Object));
        }

        [TestMethod]
        public void Throws_NullException_When_Passed_Null_SessionManager()
        {
            //Arrange
            var userManagerMock = new Mock<IUserManager>();
            var sessionManagerMock = new Mock<ISessionManager>();

            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(
                () => new ModifyUserCommand(
                userManagerMock.Object,
                null));
        }
    }
}
