﻿using Core.Commands.BorrowingBooks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.CheckOutBookCommandClassTests
{
    [TestClass]
    public class ConstructorShould
    {
        //Arrange
        Mock<ISessionManager> sessionMangerMock;
        Mock<ILibraryManager> libraryMangerMock;
        Mock<IBookItemManager> bookItemMangerMock;
        Mock<IUserManager> userMangerMock;

        public ConstructorShould()
        {
            this.sessionMangerMock = new Mock<ISessionManager>();
            this.libraryMangerMock = new Mock<ILibraryManager>();
            this.bookItemMangerMock = new Mock<IBookItemManager>();
            this.userMangerMock = new Mock<IUserManager>();
        }

        [TestMethod]
        public void CorrectlyInstantiateNewCommand_WhenCorrectValuesArePassed()
        {
             //Act
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);
            //Assert
            Assert.IsInstanceOfType(sut, typeof(CheckOutBookCommand));
        }

        [TestMethod]
        public void ThrowArgumentNullException_WhenThePassedSessionManagerIsNull()
        {
            // Act & Assert
            Assert.ThrowsException<ArgumentNullException>(() => new CheckOutBookCommand
            (null, libraryMangerMock.Object,
            bookItemMangerMock.Object, userMangerMock.Object));
        }

        [TestMethod]
        public void ThrowArgumentNullException_WhenThePassedLibraryManagerIsNull()
        {
            // Act & Assert
            Assert.ThrowsException<ArgumentNullException>(() => new CheckOutBookCommand
            (sessionMangerMock.Object, null,
            bookItemMangerMock.Object, userMangerMock.Object));
        }

        [TestMethod]
        public void ThrowArgumentNullException_WhenThePassedBookItemManagerIsNull()
        {
            // Act & Assert
            Assert.ThrowsException<ArgumentNullException>(() => new CheckOutBookCommand
            (sessionMangerMock.Object, libraryMangerMock.Object,
            null, userMangerMock.Object));
        }

        [TestMethod]
        public void ThrowArgumentNullException_WhenThePassedUserMangerIsNull()
        {
            // Act & Assert
            Assert.ThrowsException<ArgumentNullException>(() => new CheckOutBookCommand
            (sessionMangerMock.Object, libraryMangerMock.Object,
            bookItemMangerMock.Object, null));
        }


    }
}
