﻿using Core.Commands.BorrowingBooks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Common;
using Models.Contracts;
using Moq;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.CheckOutBookCommandClassTests
{
    [TestClass]
    public class ExecuteShould
    {
        //Arrange
        Mock<ISessionManager> sessionMangerMock;
        Mock<ILibraryManager> libraryMangerMock;
        Mock<IBookItemManager> bookItemMangerMock;
        Mock<IUserManager> userMangerMock;

        public ExecuteShould()
        {
            this.sessionMangerMock = new Mock<ISessionManager>();
            this.libraryMangerMock = new Mock<ILibraryManager>();
            this.bookItemMangerMock = new Mock<IBookItemManager>();
            this.userMangerMock = new Mock<IUserManager>();
        }

        [TestMethod]
        public void CallLibraryManagerCheckOutBookWithCorrectParams_WhenUserIsMemeber()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("Pesho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Member);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho");
            userMangerMock.Setup(u => u.GetUser("pesho")).Returns(userMock.Object);
            libraryMangerMock.Setup(l => l.CheckOutBook(bookToCheckOut.Object, userMock.Object))
            .Verifiable();
            //Act
            sut.Execute("15");
            //Assert
            libraryMangerMock.Verify(l => l.CheckOutBook(bookToCheckOut.Object, userMock.Object), Times.Once);
        }

        [TestMethod]
        public void CallLibraryManagerCheckOutBookWithCorrectParams_WhenUserIsLibrarian()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("Gosho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Librarian);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho");
            userMangerMock.Setup(u => u.GetUser("pesho")).Returns(userMock.Object);
            libraryMangerMock.Setup(l => l.CheckOutBook(bookToCheckOut.Object, userMock.Object))
            .Verifiable();
            //Act
            sut.Execute("15 pesho");
            //Assert
            libraryMangerMock.Verify(l => l.CheckOutBook(bookToCheckOut.Object, userMock.Object), Times.Once);
        }

        [TestMethod]
        public void CallBookItemChangeBookStatus_WhenUserIsLibrarian()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("Gosho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Librarian);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho");
            userMangerMock.Setup(u => u.GetUser("pesho")).Returns(userMock.Object);
            libraryMangerMock.Setup(l => l.CheckOutBook(bookToCheckOut.Object, userMock.Object))
            .Verifiable();
            bookToCheckOut.Setup(b => b.ChangeBookStatus(BookStatus.Loaned)).Verifiable();
            //Act
            sut.Execute("15 pesho");
            //Assert
            bookToCheckOut.Verify(b => b.ChangeBookStatus(BookStatus.Loaned), Times.Once);
        }

        [TestMethod]
        public void CallBookItemChangeBookStatus_WhenUserIsMember()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("pesho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Member);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho");
            userMangerMock.Setup(u => u.GetUser("pesho")).Returns(userMock.Object);
            libraryMangerMock.Setup(l => l.CheckOutBook(bookToCheckOut.Object, userMock.Object))
            .Verifiable();
            bookToCheckOut.Setup(b => b.ChangeBookStatus(BookStatus.Loaned)).Verifiable();
            //Act
            sut.Execute("15");
            //Assert
            bookToCheckOut.Verify(b => b.ChangeBookStatus(BookStatus.Loaned), Times.Once);
        }

        [TestMethod]
        public void CallBookItemManagerModifyBookItemMethod_WhenUserIsLibrarian()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("Gosho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Librarian);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho");
            userMangerMock.Setup(u => u.GetUser("pesho")).Returns(userMock.Object);
            libraryMangerMock.Setup(l => l.CheckOutBook(bookToCheckOut.Object, userMock.Object))
            .Verifiable();
            bookToCheckOut.Setup(b => b.ChangeBookStatus(BookStatus.Loaned)).Verifiable();
            bookItemMangerMock.Setup(b => b.ModifyBookItem(bookToCheckOut.Object)).Verifiable();
            //Act
            sut.Execute("15 pesho");
            //Assert
            bookItemMangerMock.Verify(b => b.ModifyBookItem(bookToCheckOut.Object), Times.Once);
        }

        [TestMethod]
        public void CallBookItemManagerModifyBookItemMethod_WhenUserIsMember()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("pesho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Member);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho");
            userMangerMock.Setup(u => u.GetUser("pesho")).Returns(userMock.Object);
            libraryMangerMock.Setup(l => l.CheckOutBook(bookToCheckOut.Object, userMock.Object))
            .Verifiable();
            bookToCheckOut.Setup(b => b.ChangeBookStatus(BookStatus.Loaned)).Verifiable();
            bookItemMangerMock.Setup(b => b.ModifyBookItem(bookToCheckOut.Object)).Verifiable();
            //Act
            sut.Execute("15");
            //Assert
            bookItemMangerMock.Verify(b => b.ModifyBookItem(bookToCheckOut.Object), Times.Once);
        }

        [TestMethod]
        public void ReturnCorrectMessage_WhenUserIsLibrarian()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("Gosho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Librarian);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho");
            userMock.Setup(u => u.Name).Returns("Pesho");
            userMangerMock.Setup(u => u.GetUser("pesho")).Returns(userMock.Object);
            //Act
            var result= sut.Execute("15 pesho");
            //Assert
            Assert.AreEqual("Pesho successfully checked out book: ID 15 zzzzzzzzzzz",result);
        }

        [TestMethod]
        public void ReturnCorrectMessage_WhenUserIsMember()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("Gosho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Librarian);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho2");
            userMock.Setup(u => u.Name).Returns("Pesho2");
            userMangerMock.Setup(u => u.GetUser("pesho2")).Returns(userMock.Object);
            libraryMangerMock.Setup(l => l.CheckOutBook(bookToCheckOut.Object, userMock.Object))
            .Verifiable();
            bookToCheckOut.Setup(b => b.ChangeBookStatus(BookStatus.Loaned)).Verifiable();
            bookItemMangerMock.Setup(b => b.ModifyBookItem(bookToCheckOut.Object)).Verifiable();
            //Act
           var result= sut.Execute("15 pesho2");
            //Assert
            Assert.AreEqual("Pesho2 successfully checked out book: ID 15 zzzzzzzzzzz", result);
        }

        [TestMethod]
        public void CallVerifyAuthorityMethod_WhenUserIsLibrarian()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("Gosho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Librarian);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho");
            userMangerMock.Setup(u => u.GetUser("pesho")).Returns(userMock.Object);
            sessionMangerMock.Setup(s => s.ValidateUserAuthority(userMock.Object)).Verifiable();
            
            //Act
            sut.Execute("15 pesho");
            //Assert
            sessionMangerMock.Verify(s => s.ValidateUserAuthority(userMock.Object), Times.Once);
        }

        [TestMethod]
        public void CallVerifyAuthorityMethod_WhenUserIsMember()
        {
            //Arrange
            var sut = new CheckOutBookCommand(sessionMangerMock.Object, libraryMangerMock.Object,
                bookItemMangerMock.Object, userMangerMock.Object);

            sessionMangerMock.Setup(s => s.LoggedUser.LibraryCardUserName).Returns("pesho");
            sessionMangerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Member);
            var bookToCheckOut = new Mock<IBookItem>();
            bookToCheckOut.Setup(b => b.Id).Returns(15);
            bookToCheckOut.Setup(b => b.Book.Title).Returns("zzzzzzzzzzz");
            bookItemMangerMock.Setup(b => b.GetBookItem(15)).Returns(bookToCheckOut.Object);
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.LibraryCardUserName).Returns("pesho");
            userMangerMock.Setup(u => u.GetUser("pesho")).Returns(userMock.Object);
            sessionMangerMock.Setup(s => s.ValidateUserAuthority(userMock.Object)).Verifiable();

            //Act
            sut.Execute("15");
            //Assert
            bookToCheckOut.Verify(b => b.ChangeBookStatus(BookStatus.Loaned), Times.Once);
            sessionMangerMock.Verify(s => s.ValidateUserAuthority(userMock.Object), Times.Once);
        }

    }
}
