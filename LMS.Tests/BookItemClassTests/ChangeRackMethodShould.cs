﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Models.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

 namespace LMS.Tests.BookItemClassTests
{
    [TestClass]
    public class ChangeRackMethodShould

    {

        Mock<IBook> bookMock;

        public ChangeRackMethodShould()
        {
            this.bookMock = new Mock<IBook>();
        }

        [TestMethod]
        public void CorrectlyChangeRack_WhenValidValueIsPassed()
        {
            //Arrange
            var sut = new BookItem(15, bookMock.Object, 6);
            //Act
            sut.ChangeRack(10);
            //Assert
            Assert.AreEqual(10, sut.RackNumber);
        }

        [DataTestMethod]
        [DataRow(-1)]
        [DataRow(500000001)]
        public void ThrowException_WhenInvalidValueIsPassed(int rackNumber)
        {
            //Arrange
            var sut = new BookItem(15, bookMock.Object, 6);
            //Act, Assert
            Assert.ThrowsException<ArgumentException>(()=> sut.ChangeRack(rackNumber));
        }

    }
}
