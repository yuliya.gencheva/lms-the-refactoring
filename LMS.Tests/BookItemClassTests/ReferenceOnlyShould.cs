﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Models.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.BookItemClassTests
{
    [TestClass]
    public class ReferenceOnlyShould
    {
        Mock<IBook> bookMock;

        public ReferenceOnlyShould()
        {
            this.bookMock = new Mock<IBook>();
        }

        [TestMethod]
        public void CorrectlyChangeReference_WhenValidValueIsPassed()
        {
            //Arrange
            var sut = new BookItem(15, bookMock.Object, 6);
            //Act
            sut.ReferenceOnly=true;
            //Assert
            Assert.AreEqual(true, sut.ReferenceOnly);
        }

    }
}
