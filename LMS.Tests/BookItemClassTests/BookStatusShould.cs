﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Models.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.BookItemClassTests
{
    [TestClass]
    public class BookStatusShould
    {

        [TestMethod]
        public void CorrectlyChangeBookStatus_WhenValidValueIsPassed()
        {
            //Arrange
            Mock<IBook> bookMock = new Mock<IBook>();
            var sut = new BookItem(15, bookMock.Object, 6);
            //Act
            sut.ChangeBookStatus(Models.Common.BookStatus.Available);
            //Assert
            Assert.AreEqual(Models.Common.BookStatus.Available, sut.BookStatus);
        }

    }
}
