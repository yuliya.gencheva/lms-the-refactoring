﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Models.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.BookItemClassTests
{
    [TestClass]
    public class ConstructorShould
    {
        [TestMethod]
        public void CorrectlyAssignID_WhenValidValuesPassed() {
            //Arrange
            var bookMock = new Mock<IBook>();
            //Act
            var sut = new BookItem(17, bookMock.Object, 5 );
            //Assert
            Assert.AreEqual(17, sut.Id);
        }

        [TestMethod]
        public void CorrectlyAssignBook_WhenValidValuesPassed()
        {
            //Arrange
            var bookMock = new Mock<IBook>();
            bookMock.Setup(b => b.Title).Returns("MyMockedBook");
            //Act
            var sut = new BookItem(17, bookMock.Object, 5);
            //Assert
            Assert.AreEqual("MyMockedBook", sut.Book.Title);
        }

        [TestMethod]
        public void CorrectlyAssignRackNumber_WhenValidValuesPassed()
        {
            //Arrange
            var bookMock = new Mock<IBook>();
            //Act
            var sut = new BookItem(17, bookMock.Object, 5);
            //Assert
            Assert.AreEqual(5, sut.RackNumber);
        }

        [TestMethod]
        public void CorrectlyInstantiateObject_WhenValidValuesPassed()
        {
            //Arrange
            var bookMock = new Mock<IBook>();
            //Act
            var sut = new BookItem(17, bookMock.Object, 5);
            //Assert
            Assert.IsInstanceOfType(sut, typeof(IBookItem));
        }

        [TestMethod]
        public void ThrowException_WhenPassedBookIsNull()
        { 
            //Act&Assert
            Assert.ThrowsException<ArgumentNullException>(()=>new BookItem(17,null, 5));
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(201)]
        public void ThrowException_WhenRackNumberIsInvalid(int rackNumber)
        {
            //Act&Assert
            Assert.ThrowsException<ArgumentException>(() => new BookItem(17, new Mock<IBook>().Object, rackNumber));
        }

        [DataTestMethod]
        [DataRow(0)]
        [DataRow(500000001)]
        public void ThrowException_WhenIdIsInvalid(int id)
        {
            //Act&Assert
            Assert.ThrowsException<ArgumentException>(() => new BookItem(id, new Mock<IBook>().Object, 7));
        }


    }
}
