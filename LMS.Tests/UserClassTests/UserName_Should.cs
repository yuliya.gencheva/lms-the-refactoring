﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Common;
using Models.Models;
using System;

namespace LMS.Tests.UserClassTests
{
    [TestClass]
    public class UserName_Should
    {
        [TestMethod]
        public void Add_Correct_UserName_When_Passed_Valid_Values()
        {
            //Arrange
            string name = "Gosho";
            string userName = "Tosho";
            Role role = Role.Member;

            //Act
            var sut = new User(name, userName, role);

            //Assert
            Assert.AreEqual(userName, sut.LibraryCardUserName);
        }

        [TestMethod]
        public void Throw_NullException_When_Passed_NullValue()
        {
            //Arrange
            string name = "Gosho";
            Role role = Role.Member;

            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => new User(name, null, role));
        }

        [TestMethod]
        public void Throw_ArgumentException_When_Passed_Over_Maxed_Length()
        {
            //Arrange
            string name = "Gosho";
            string userName = new string('a', 16);
            Role role = Role.Member;

            //Act

            //Assert
            ArgumentException msg = Assert.ThrowsException<ArgumentException>(() => new User(name, userName, role));
        }

        [TestMethod]
        public void Throw_ArgumentException_CorrectMessage_When_Over_Maxed_Length()
        {
            //Arrange
            string name = "Gosho";
            string userName = new string('a', 16);
            Role role = Role.Member;
            string msgTest = "Value must be between 3 and 15 characters long!";

            //Act
            ArgumentException msg = Assert.ThrowsException<ArgumentException>(() => new User(name, userName, role));

            //Assert
            Assert.AreEqual(msgTest, msg.Message);
        }

        [TestMethod]
        public void Throw_ArgumentException_When_Passed_Under_Min_Length()
        {
            //Arrange
            string name = "Gosho";
            string userName = "aa";
            Role role = Role.Member;

            //Act

            //Assert
            ArgumentException msg = Assert.ThrowsException<ArgumentException>(() => new User(name, userName, role));
        }

        [TestMethod]
        public void Throw_ArgumentException_CorrectMessage_When_Under_Min_Length()
        {
            //Arrange
            string name = "Gosho";
            string userName = "aa";
            Role role = Role.Member;
            string msgTest = "Value must be between 3 and 15 characters long!";

            //Act
            ArgumentException msg = Assert.ThrowsException<ArgumentException>(() => new User(name, userName, role));

            //Assert
            Assert.AreEqual(msgTest, msg.Message);
        }
    }
}
