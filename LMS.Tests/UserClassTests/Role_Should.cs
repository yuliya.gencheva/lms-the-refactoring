﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Common;
using Models.Models;

namespace LMS.Tests.UserClassTests
{
    [TestClass]
    public class Role_Should
    {
        [TestMethod]
        public void Set_Role_When_Passed_Valid_Value()
        {
            //Arrange
            string name = "Gosho";
            string userName = "Tosho";
            Role role = Role.Member;

            //Act
            var sut = new User(name, userName, role);

            //Assert
            Assert.AreEqual(role, sut.Role);
        }
    }
}
