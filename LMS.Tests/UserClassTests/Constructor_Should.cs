﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Common;
using Models.Models;

namespace LMS.Tests.UserClassTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Create_Instance_When_Valide_Values_Passed()
        {
            //Arrange
            string name = "Gosho";
            string libraryCardUserName = "Tosho";
            Role role = Role.Member;

            //Act
            var sut = new User(name, libraryCardUserName, role);

            //Assert
            Assert.IsInstanceOfType(sut, typeof(User));
        }
    }
}
