﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services;
using Services.Contracts;

namespace LMS.Tests.UserManagerTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void CorrectlyInstantiateObject_WhenValidValuesPassed()
        {
            //Arrange
            var userDatabase = new Mock<IUserDatabase>();
            var libraryManager = new Mock<ILibraryManager>();
            //Act
            var sut = new UserManager(userDatabase.Object, libraryManager.Object);
            //Assert
            Assert.IsInstanceOfType(sut, typeof(IUserManager));
        }
    }
}
