﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Contracts;
using Moq;
using Services;
using Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Tests.UserManagerTests
{
    [TestClass]
    public class ModifyUser_Should
    {
        Mock<ILibraryManager> _libraryMock;
        Mock<IUserDatabase> _userItemDatabaseMock;
        public ModifyUser_Should()
        {
            this._libraryMock = new Mock<ILibraryManager>();
            this._userItemDatabaseMock = new Mock<IUserDatabase>();
        }

        [TestMethod]
        public void Call_UserDatabase_When_Valid_User_Passed()
        {
            // Arrange
            var userMock = new Mock<IUser>();
            this._userItemDatabaseMock.Setup(b => b.Modify(userMock.Object)).Verifiable();
            var sut = new UserManager(this._userItemDatabaseMock.Object, this._libraryMock.Object);

            //Act
            sut.ModifyUser(userMock.Object);

            //Assert
            this._userItemDatabaseMock.Verify((b) => b.Modify(userMock.Object), Times.Once);
        }

        [TestMethod]
        public void Throw_NullException_When_NullUser()
        {
            //Arrange
            var userMock = new Mock<IUser>();
            var mockListUsers = new List<IUser>();
            mockListUsers.Add(userMock.Object);
            this._userItemDatabaseMock.Setup(u => u.Users).Returns(mockListUsers);
            var sut = new UserManager(this._userItemDatabaseMock.Object, this._libraryMock.Object);

            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.ModifyUser(null));
        }
    }
}
