﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Common;
using Models.Contracts;
using Models.Models;
using Moq;
using Services;
using Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Tests.UserManagerTests
{
    //Created by Yordan/Julia
    //Това за пробно. Така ли да вписваме кое е от нас или нещо подобно
    //от рода на Implemented by Yordan/Julia
    [TestClass]
    public class AddNewUser_Should
    {
        Mock<ILibraryManager> libraryMock;
        Mock<IUserDatabase> userItemDatabaseMock;
        public AddNewUser_Should()
        {
            this.libraryMock = new Mock<ILibraryManager>();
            this.userItemDatabaseMock = new Mock<IUserDatabase>();
            
        }
        [TestMethod]
        public void Pass_User_When_Valid()
        {
            //Arrange
            var userMock = new Mock<IUser>();
            var mockListUsers = new List<IUser>();
            this.userItemDatabaseMock.Setup(b => b.Add(userMock.Object)).Verifiable();
            this.userItemDatabaseMock.Setup(b => b.Users).Returns(mockListUsers);
            var sut = new UserManager(this.userItemDatabaseMock.Object, this.libraryMock.Object);
            
            //Act

            sut.AddNewUser(userMock.Object);
            //Assert
            this.userItemDatabaseMock.Verify((b) => b.Add(userMock.Object), Times.Once);
        }

        [TestMethod]
        public void Throw_Exception_When_User_Exists()
        {
            //Arrange
            var userMock = new Mock<User>("Gosho", "Tosho", Role.Member);
            var mockListUsers = new List<IUser>();
            mockListUsers.Add(userMock.Object);
            this.userItemDatabaseMock.Setup(u => u.Users).Returns(mockListUsers);
            var sut = new UserManager(this.userItemDatabaseMock.Object, this.libraryMock.Object);

            //Act

            //Assert
            Assert.ThrowsException<ArgumentException>(() => sut.AddNewUser(userMock.Object));
        }

        [TestMethod]
        public void Throw_Null_Exception_When_User_Null()
        {
            //Arrange
            var userMock = new Mock<User>("Gosho", "Tosho", Role.Member);
            var mockListUsers = new List<IUser>();
            mockListUsers.Add(userMock.Object);
            this.userItemDatabaseMock.Setup(u => u.Users).Returns(mockListUsers);
            var sut = new UserManager(this.userItemDatabaseMock.Object, this.libraryMock.Object);

            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.AddNewUser(null));
        }
    }
}
