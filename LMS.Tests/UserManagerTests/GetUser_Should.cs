﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Common;
using Models.Contracts;
using Models.Models;
using Moq;
using Services;
using Services.Contracts;
using System;
using System.Collections.Generic;

namespace LMS.Tests.UserManagerTests
{
    [TestClass]
    public class GetUser_Should
    {
        Mock<ILibraryManager> _libraryMock;
        Mock<IUserDatabase> _userItemDatabaseMock;
        public GetUser_Should()
        {
            this._libraryMock = new Mock<ILibraryManager>();
            this._userItemDatabaseMock = new Mock<IUserDatabase>();
        }
        [TestMethod]
        public void Return_Valid_User()
        {
            //Arrange
            var userMock = new Mock<User>("Gosho", "Tosho", Role.Member);
            var mockListUsers = new List<IUser>();
            mockListUsers.Add(userMock.Object);
            this._userItemDatabaseMock.Setup(u => u.Users).Returns(mockListUsers);
            var sut = new UserManager(this._userItemDatabaseMock.Object, this._libraryMock.Object);

            //Act

            //Assert
            Assert.AreEqual("Gosho", sut.GetUser(userMock.Object.LibraryCardUserName).Name);
            Assert.AreEqual("Tosho", sut.GetUser(userMock.Object.LibraryCardUserName).LibraryCardUserName);
            Assert.AreEqual(Role.Member, sut.GetUser(userMock.Object.LibraryCardUserName).Role);
        }

        [TestMethod]
        public void Throw_Correct_Message_When_Invalid()
        {
            //Arrange
            var testName = "Pesho";
            var userMock = new Mock<User>("Gosho", "Tosho", Role.Member);
            var mockListUsers = new List<IUser>();
            mockListUsers.Add(userMock.Object);
            this._userItemDatabaseMock.Setup(u => u.Users).Returns(mockListUsers);
            var sut = new UserManager(this._userItemDatabaseMock.Object, this._libraryMock.Object);

            //Act
            var ex = Assert.ThrowsException<ArgumentNullException>(() => sut.GetUser(testName));

            //Assert
            Assert.AreEqual($"No user with name: {testName} exists in Database!", ex.Message);
        }

        [TestMethod]
        public void Throw_NullException_When_Passed_NullUser()
        {
            //Arrange
            var userMock = new Mock<IUser>();
            var mockListUsers = new List<IUser>();
            mockListUsers.Add(userMock.Object);
            this._userItemDatabaseMock.Setup(u => u.Users).Returns(mockListUsers);
            var sut = new UserManager(this._userItemDatabaseMock.Object, this._libraryMock.Object);

            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.AddNewUser(null));
        }
    }
}
