﻿using FileDatabase.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Common;
using Models.Contracts;
using Models.Models;
using Moq;
using Services;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Tests.UserManagerTests
{
    [TestClass]
    public class RemoveUser_Should
    {
        Mock<ILibraryManager> _libraryMock;
        Mock<IUserDatabase> _userItemDatabaseMock;
        public RemoveUser_Should()
        {
            this._libraryMock = new Mock<ILibraryManager>();
            this._userItemDatabaseMock = new Mock<IUserDatabase>();
        }

        [TestMethod]
        public void Throw_NullException_When_Passed_NullUser()
        {
            //Arrange
            var userMock = new Mock<IUser>();
            var mockListUsers = new List<IUser>();
            mockListUsers.Add(userMock.Object);
            this._userItemDatabaseMock.Setup(u => u.Users).Returns(mockListUsers);
            var sut = new UserManager(this._userItemDatabaseMock.Object, this._libraryMock.Object);

            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => sut.RemoveUser(null));
        }

        [TestMethod]
        public void Call_UserDatabase_When_Valid_User_Passed()
        {
            //Arrange
            var userMock = new Mock<User>("Gosho", "Tosho", Role.Member);
            var sut = new UserManager(this._userItemDatabaseMock.Object, this._libraryMock.Object);

            //Act
            sut.RemoveUser(userMock.Object);

            //Assert
            this._userItemDatabaseMock.Verify((b) => b.Remove(userMock.Object), Times.Once);
        }
    }
}
