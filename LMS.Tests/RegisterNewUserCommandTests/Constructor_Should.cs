﻿using Core.Commands.ModifyUsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services;
using Services.Contracts;
using Services.Factories;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.RegisterNewUserCommandTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Correctly_Instantiate_NewCommand_When_Valid_Values_Passed()
        {
            //Arrange
            var userManagerMock = new Mock<IUserManager>();
            var userFactoryMock = new Mock<IUserFactory>();
            var sessionManagerMock = new Mock<ISessionManager>();

            //Act
            var sut = new RegisterNewUserCommand(
                userManagerMock.Object,
                userFactoryMock.Object,
                sessionManagerMock.Object);

            //Assert
            Assert.IsInstanceOfType(sut, typeof(RegisterNewUserCommand));
        }

        [TestMethod]
        public void Throws_NullException_When_Passed_Null_UserManager()
        {
            //Arrange
            var userManagerMock = new Mock<IUserManager>();
            var userFactoryMock = new Mock<IUserFactory>();
            var sessionManagerMock = new Mock<ISessionManager>();
            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => new RegisterNewUserCommand(
                null,
                userFactoryMock.Object,
                sessionManagerMock.Object));
        }

        [TestMethod]
        public void Throws_NullException_When_Passed_Null_UserFactory()
        {
            //Arrange
            var userManagerMock = new Mock<IUserManager>();
            var userFactoryMock = new Mock<IUserFactory>();
            var sessionManagerMock = new Mock<ISessionManager>();
            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => new RegisterNewUserCommand(
                userManagerMock.Object,
                null,
                sessionManagerMock.Object));
        }

        [TestMethod]
        public void Throws_NullException_When_Passed_Null_SessionManager()
        {
            //Arrange
            var userManagerMock = new Mock<IUserManager>();
            var userFactoryMock = new Mock<IUserFactory>();
            var sessionManagerMock = new Mock<ISessionManager>();
            //Act

            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => new RegisterNewUserCommand(
                userManagerMock.Object,
                userFactoryMock.Object,
                null));
        }
    }
}
