﻿using Core.Commands.ModifyUsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.Common;
using Models.Contracts;
using Moq;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Tests.RegisterNewUserCommandTests
{
    [TestClass]
    public class Execute_Should
    {
        [TestMethod]
        public void Call_UserFactory_CreateUser_When_Valid_Parameters()
        {
            //Arrange
            var name = "Gosho";
            var userName = "Tosho";
            var role = Role.Member;
            var userMock = new Mock<IUser>();
            userMock.Setup(u => u.Name).Returns(name);
            userMock.Setup(u => u.LibraryCardUserName).Returns(userName);
            userMock.Setup(u => u.Role).Returns(role);
            var userManagerMock = new Mock<IUserManager>();
            var userFactoryMock = new Mock<IUserFactory>();
            userFactoryMock.Setup(uF => uF.CreateUser(name, userName, role)).Returns(userMock.Object);
            var sessionManagerMock = new Mock<ISessionManager>();
            sessionManagerMock.Setup(s => s.LoggedUser.Role).Returns(Role.Librarian);

            var sut = new RegisterNewUserCommand(
                userManagerMock.Object,
                userFactoryMock.Object,
                sessionManagerMock.Object);
            //Act
            sut.Execute("Gosho, Tosho, Member");
            //Assert
            userManagerMock.Verify(m => m.AddNewUser(userMock.Object), Times.Once);
        }

        [TestMethod]
        public void Throw_ArgumentException_When_Invalid_Params_Passed()
        {
            //Arrange
            var paramesString = "bacd, Member";
            var userManagerMock = new Mock<IUserManager>();
            var userFactoryMock = new Mock<IUserFactory>();
            var sessionManagerMock = new Mock<ISessionManager>();
            var sut = new RegisterNewUserCommand(
                userManagerMock.Object,
                userFactoryMock.Object,
                sessionManagerMock.Object);

            //Act

            //Assert
            Assert.ThrowsException<ArgumentException>(() => sut.Execute(paramesString));
        }
    }
}
