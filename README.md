# Library Management System Julia&Jordan


![](Images/Cover.jpg)

### Trinity College Dublin Library, Ireland  
Famed for its Long Room, the library of Trinity College library is home to the largest collection of books in Ireland.  
The over 200-foot-long main room is covered in marble and dark wood pilasters.  
When it was first completed, the Long Room had a flat ceiling, but the roof was raised to accommodate more books.  

The system offers several commands to support library work.  
A user must be logged in for the system to be able to operate and receive commands.  

## How to use commands:  

### Each librarian can use the following commands in the described format:
- `searchbooks/[title/author/category/year]/[parameters]` - search books by given
title, author, category or year
- `checkoutbook/[bookItemId]/[UserName]` - check out book for specified user  
- `renewbook/[bookItemId]/[UserName]` - renew book  for specified user
- `reservebook/[bookItemId]/[UserName]` - make a book reservation for specified user
- `returnbook/[bookItemId]/[UserName]` - returning book for specified user  
- `login/[UserName]` login with user name   
- `logout` - logout from the current user name  
- `registernewuser/[name]/[UserName]/[RoleId]` -  registration of a new member; RoleId: 1-librarian,2-member 
- `addbook/[title]/[author]/[pages]/[year]/[country]/[language]/[link]/[isbn]/[category]` - adding book  
- `removebook/[title]/[author]/[year]` - remove book  
- `addbookitem/[title]/[author]/[year]/[rack]` - add book item  
- `removebookitem/[bookItemId]` - remove book item   
- `modifybookitem/[bookItemId]/[racknumber/referenceonly/bookStatus]/[Parameters]` - modify book item by rack number, as referenceonly or bookStatus   
- `modifyuser/[newName]` - modify own name  
- `cancelmembership/[userName]` - cancle membership by given user name
- `libraryinfo` - shows information about the library  
- `viewaccount` - shows current status of the user   

### Each member can use the following commands in the described format:  
- `login/[UserName]` - login with user name 
- `logout` - logout from the current user name
- `searchbooks/[title/author/category/year]/[parameters]` - search books by given title, author, category or year
- `checkoutbook/[bookItemId]` - check out book 
- `renewbook/[bookItemId]` - renew book
- `reservebook/[bookItemId]` - make a book reservation 
- `returnbook/[bookItemId]` - returning book
- `modifyuser/[newName]` - modify name
- `cancelmembership/[userName]` - cancel membership by given user name
- `libraryinfo` - shows information about the library
- `viewaccount` - shows current status of the user

### Some example input: 
> login/JordanG  
> searchbooks/title/Ficciones  
> checkoutbook/11/Ficheto  
> renewbook/11/Ficheto  
> reservebook/12/Ficheto  
> returnbook/11/Ficheto  
> registernewuser/Kotio/O/1  
> registernewuser/1111111111111111111111111111111/ohoh/1  
> registernewuser/Kotio/Kotio/1  
> logout  
> Kotio  
> logout  
> JordanG  
> addbook/Captain Blood2/Rafael Sabatini/258/1922/England/English/link/9781934648186/0  
> removebook/Things Fall Apart 3/Chinua Achebe/1958  
> removebook/Captain Blood/Rafael Sabatini/1922  
> addbookitem/Captain Blood/Rafael Sabatini/1922/10  
> removebookitem/202  
> modifybookitem/201/racknumber/10  
> modifyuser/Dancho  
> modifyuser/Petio  
> cancelmembership/Kotio
> libraryinfo  
> viewaccount  
> logout  
> end  


## Team members: 
Julia Stoimenova & Jordan Gospodinov

What problems did the application have before you started refactoring?
The program was tightly coupled. All of the SOLID principles were violated. The user needed downcasting to perform the member role. The information from the current session was not saved after closing.

What design patterns have you used and why?
We have used the Service Layer Design Pattern to provide communication between the Databases and the Core Layer.
Class diagram of your types

![alt text](https://gitlab.com/yuliya.gencheva/lms-the-refactoring/raw/master/Images/CodeMap.jpg)


