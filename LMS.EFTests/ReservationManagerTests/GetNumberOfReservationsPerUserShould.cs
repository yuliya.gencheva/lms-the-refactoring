﻿using LMS.Data;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.EFTests.ReservationManagerTests
{
    [TestClass]
    public class GetNumberOfReservationsPerUserShould
    {
        Mock<IBookItemManager> bookItemManagerMock;
        Mock<IUserManager> userManagerMock;
        Mock<IFineManager> fineManagerMock;
        Mock<IBookReservationFactory> bookReservationFactoryMock;


        public GetNumberOfReservationsPerUserShould()
        {
            bookItemManagerMock = new Mock<IBookItemManager>();
            userManagerMock = new Mock<IUserManager>();
            fineManagerMock = new Mock<IFineManager>();
            bookReservationFactoryMock = new Mock<IBookReservationFactory>();
        }

        [TestMethod]
        public void ReturnCorrectNumberOfBookReservations()
        {

            int bookItemId = 15;
            int bookItemId2 = 16;
            var user = new User { Id = 6, LibraryCardUserName = "pesho" };

            var options = TestUtilities.GetOptions(nameof(ReturnCorrectNumberOfBookReservations));

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BookReservations.Add(new BookReservation()
                {
                    BookItemId = bookItemId,
                    User = user
                });

                arrangeContext.BookReservations.Add(new BookReservation()
                {
                    BookItemId = bookItemId2,
                    User = user
                });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new ReservationManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                    fineManagerMock.Object, bookReservationFactoryMock.Object);
                var count = sut.GetNumberOfReservationsPerUser(user);
                Assert.AreEqual(2, count);
            }
        }

        [TestMethod]
        public void ThrowException_WhenPassedUserIsNull()
        {

            int bookItemId = 15;
            int bookItemId2 = 16;
            User user = null;

            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenPassedUserIsNull));

            using (var assertContext = new LMSContext(options))
            {
                var sut = new ReservationManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                    fineManagerMock.Object, bookReservationFactoryMock.Object);

                Assert.ThrowsException<ArgumentNullException>(() => sut.GetNumberOfReservationsPerUser(user));
            }
        }

        [TestMethod]
        public void ThrowExceptionWithCorrectMesssage_WhenPassedUserIsNull()
        {

            int bookItemId = 15;
            int bookItemId2 = 16;
            var message = "Invalid user!";
            User user = null;

            var options = TestUtilities.GetOptions(nameof(ThrowExceptionWithCorrectMesssage_WhenPassedUserIsNull));

            using (var assertContext = new LMSContext(options))
            {
                var sut = new ReservationManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                   fineManagerMock.Object, bookReservationFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentNullException>(() => sut.GetNumberOfReservationsPerUser(user));
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}
