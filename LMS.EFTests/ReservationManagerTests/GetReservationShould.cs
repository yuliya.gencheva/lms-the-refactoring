﻿using LMS.Data;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.EFTests.LibararyManagerTests
{
    [TestClass]
    public class GetReservationShould
    {
        Mock<IBookItemManager> bookItemManagerMock;
        Mock<IUserManager> userManagerMock;
        Mock<IFineManager> fineManagerMock;
        Mock<IBookReservationFactory> bookReservationFactoryMock;


        public GetReservationShould()
        {
            bookItemManagerMock = new Mock<IBookItemManager>();
            userManagerMock = new Mock<IUserManager>();
            fineManagerMock = new Mock<IFineManager>();
            bookReservationFactoryMock = new Mock<IBookReservationFactory>();
        }

        [TestMethod]
        public void GetCorrectReservation()
        {
            int bookItemId = 15;
            var user1 = new User { Id = 6 };
            var user2 = new User { Id = 7 };
            var options = TestUtilities.GetOptions(nameof(GetCorrectReservation));
           
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BookReservations.Add(new BookReservation()
                {
                    BookItemId = bookItemId,
                    User = user1
                });

                arrangeContext.BookReservations.Add(new BookReservation()
                {
                    BookItemId = bookItemId,
                    User = user2
                });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new ReservationManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                   fineManagerMock.Object, bookReservationFactoryMock.Object);
                var reservation = sut.GetReservation(bookItemId);
                Assert.AreEqual(user1.Id, reservation.UserId);
            }

        }

        [TestMethod]
        public void ThrowException_WhenBookReservationDoesNotExist()
        {
            int existingBookItemId = 15;
            int notexistingBookItemId = 16;
            var user1 = new User { Id = 6 };
            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenBookReservationDoesNotExist));

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BookReservations.Add(new BookReservation()
                {
                    BookItemId = existingBookItemId,
                    User = user1
                });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new ReservationManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                   fineManagerMock.Object, bookReservationFactoryMock.Object);

                Assert.ThrowsException<ArgumentNullException>(() => sut.GetReservation(notexistingBookItemId));
            }
        }

        [TestMethod]
        public void ThrowCorrectExceptionMessage_WhenBookReservationDoesNotExist()
        {
            int existingBookItemId = 15;
            int notexistingBookItemId = 16;
            var user1 = new User { Id = 6 };
            var options = TestUtilities.GetOptions(nameof(ThrowCorrectExceptionMessage_WhenBookReservationDoesNotExist));
            
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BookReservations.Add(new BookReservation()
                {
                    BookItemId = existingBookItemId,
                    User = user1
                });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new ReservationManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                    fineManagerMock.Object, bookReservationFactoryMock.Object);
                var ex = Assert.ThrowsException<ArgumentNullException>(() => sut.GetReservation(notexistingBookItemId));

                Assert.AreEqual($"BookItem with Id {notexistingBookItemId} is not reserved!", ex.Message);
            }
        }
    }
}
