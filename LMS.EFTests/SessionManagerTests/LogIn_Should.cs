﻿using LMS.Data;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace LMS.EFTests.SessionManagerTests
{
    [TestClass]
    public class LogIn_Should
    {
        [TestMethod]
        public void ThrowExeception_WhenUserNotFound()
        {
            var userName = "Gosho";
            var options = TestUtilities.GetOptions(nameof(ThrowExeception_WhenUserNotFound));
            var userManagerMock = new Mock<IUserManager>();

            using(var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(new User { LibraryCardUserName = "Pesho" });
                arrangeContext.SaveChanges();
            }

            using(var assertContext = new LMSContext(options))
            {
                var sut = new SessionManager(userManagerMock.Object);

                Assert.ThrowsException<ArgumentNullException>(() => sut.LogIn(userName));
            }
        }

        [TestMethod]
        public void ReturnCorrectMessage_WhenUserIsFound()
        {
            var user = new User("Genadi", "Gandi", 2);
            var correctMessage = $"Hello, {user.Name}!{Environment.NewLine}For a list of all available commands, write: help";
            
            var options = TestUtilities.GetOptions(nameof(ReturnCorrectMessage_WhenUserIsFound));
            var userManagerMock = new Mock<IUserManager>();
            userManagerMock.Setup(u => u.GetUser(user.Name)).Returns(user);


            var sut = new SessionManager(userManagerMock.Object);

            Assert.AreEqual(correctMessage, sut.LogIn(user.Name));
        }
    }
}
