﻿using LMS.Data;
using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace LMS.EFTests.BookManagerTests
{
    [TestClass]
    public class AddBook_Should
    {
        [TestMethod]
        public void CallCreateBook_WhenCorrectValuesArePassed()
        {
            var title = "book";
            var aurthorName = "Rafael Sabatini";
            Author authorToAdd = new Author { Name = aurthorName};
            var pages = 123;
            var year = 1988;
            var country = "Bulgaria";
            var language = "Bulgarian";
            var link = "http";
            var isbn = "973-987";
            CategoryType category = CategoryType.Detective;

            var options = TestUtilities.GetOptions(nameof(CallCreateBook_WhenCorrectValuesArePassed));
            var bookFactoryMock = new Mock<IBookFactory>();
            var book = new Book
            {
                Title = title,
                AuthorId = authorToAdd.AuthorId,
                Pages = pages,
                Year = year,
                Country = country,
                Language = language,
                Link = link,
                ISBN = isbn,
                Category = category
            };
            
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Authors.Add(authorToAdd);
                arrangeContext.SaveChanges();
            }

            bookFactoryMock.Setup(b => b.CreateBook(
                title, authorToAdd.AuthorId, 
                pages, year, 
                country, language, 
                link, isbn, 
                category))
                .Returns(book);

            using (var assertContext = new LMSContext(options))
            {
                var sut = new BookManager(assertContext, bookFactoryMock.Object);
                
                sut.AddBook(title, aurthorName, pages, year, country, language, link, isbn, category);
               
                bookFactoryMock.Verify(b => b.CreateBook(
                    title, authorToAdd.AuthorId,
                    pages, year, 
                    country, language, 
                    link, isbn, 
                    category), Times.Once);
            }
        }

        [TestMethod]
        public void ThrowException_WhenBookExists()
        {
            var title = "Blood";
            var aurthorName = "Sabatini";
            Author authorToAdd = new Author { Name = aurthorName };
            var pages = 123;
            var year = 1988;
            var country = "Bulgaria";
            var language = "Bulgarian";
            var link = "http";
            var isbn = "973-987";
            CategoryType category = CategoryType.Detective;

            var options = TestUtilities.GetOptions(nameof(CallCreateBook_WhenCorrectValuesArePassed));
            var bookFactoryMock = new Mock<IBookFactory>();
            var bookToAdd = new Book
            {
                Title = title,
                AuthorId = authorToAdd.AuthorId,
                Pages = pages,
                Year = year,
                Country = country,
                Language = language,
                Link = link,
                ISBN = isbn,
                Category = category
            };

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Books.Add(bookToAdd);
                arrangeContext.Authors.Add(authorToAdd);
                arrangeContext.SaveChanges();
            }

            bookFactoryMock.Setup(b => b.CreateBook(
                title, authorToAdd.AuthorId,
                pages, year,
                country, language,
                link, isbn,
                category))
                .Returns(bookToAdd);

            using (var assertContext = new LMSContext(options))
            {
                var sut = new BookManager(assertContext, bookFactoryMock.Object);

                Assert.ThrowsException<ArgumentException>(() => sut.AddBook(title, aurthorName, pages, year, country, language, link, isbn, category));
            }
        }
    }
}
