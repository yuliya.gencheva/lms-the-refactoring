﻿using LMS.Data;
using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace LMS.EFTests.BookManagerTests
{
    [TestClass]
    public class RemoveBook_Should
    {
        [TestMethod]
        public void ShouldRemoveBook_FromDataBase_WhenFound()
        {
            var title = "book2";
            var aurthorName = "Rafael";
            Author authorToAdd = new Author { AuthorId = 1, Name = aurthorName };
            var pages = 123;
            var year = 1988;
            var country = "Bulgaria";
            var language = "Bulgarian";
            var link = "http";
            var isbn = "973-987";
            CategoryType category = CategoryType.Detective;

            var options = TestUtilities.GetOptions(nameof(ShouldRemoveBook_FromDataBase_WhenFound));
            var bookFactoryMock = new Mock<IBookFactory>();
            var bookToRemove = new Book
            {
                Title = title,
                AuthorId = authorToAdd.AuthorId,
                Pages = pages,
                Year = year,
                Country = country,
                Language = language,
                Link = link,
                ISBN = isbn,
                Category = category
            };

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Books.Add(bookToRemove);
                arrangeContext.Authors.Add(authorToAdd);
                arrangeContext.SaveChanges();
                var sut = new BookManager(arrangeContext, bookFactoryMock.Object);
                sut.RemoveBook(title, authorToAdd.Name, year);
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new BookManager(assertContext, bookFactoryMock.Object);

                Assert.AreEqual(0, assertContext.Books.Count());
            }
        }

        [TestMethod]
        public void ThorwException_WhenBookHasCopies()
        {
            Author authorToAdd = new Author { AuthorId = 1, Name = "Rafael" };

            var options = TestUtilities.GetOptions(nameof(ThorwException_WhenBookHasCopies));
            var bookFactoryMock = new Mock<IBookFactory>();
            var bookToCheck = new Book
            {
                Title = "book3",
                AuthorId = 1,
                Pages = 123,
                Year = 1988,
                Country = "Bulgaria",
                Language = "Bulgarian",
                Link = "http",
                ISBN = "973-987",
                Category = CategoryType.Detective
            };

            var bookItemTest = new BookItem
            {
                Book = bookToCheck,
                BookStatus = BookStatus.Available,
                RackNumber = 10
            };

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Books.Add(bookToCheck);
                arrangeContext.Authors.Add(authorToAdd);
                arrangeContext.BookItems.Add(bookItemTest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new BookManager(assertContext, bookFactoryMock.Object);

                Assert.ThrowsException<ArgumentException>(() => sut.RemoveBook("book3", authorToAdd.Name, 1988));
            }
        }

        [TestMethod]
        public void ThorwCorrectMessage_WhenBookHasCopies()
        {
            var testMessage = "You must delete all copies of the book";
            Author authorToAdd = new Author { AuthorId = 2, Name = "Samuel" };

            var options = TestUtilities.GetOptions(nameof(ThorwCorrectMessage_WhenBookHasCopies));
            var bookFactoryMock = new Mock<IBookFactory>();
            var bookToCheck = new Book
            {
                Title = "book4",
                AuthorId = 2,
                Pages = 123,
                Year = 1988,
                Country = "Bulgaria",
                Language = "Bulgarian",
                Link = "http",
                ISBN = "973-987",
                Category = CategoryType.Detective
            };

            var bookItemTest = new BookItem
            {
                Book = bookToCheck,
                BookStatus = BookStatus.Available,
                RackNumber = 9
            };

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Books.Add(bookToCheck);
                arrangeContext.Authors.Add(authorToAdd);
                arrangeContext.BookItems.Add(bookItemTest);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new BookManager(assertContext, bookFactoryMock.Object);
                var message = Assert.ThrowsException<ArgumentException>(() => sut.RemoveBook("book4", authorToAdd.Name, 1988));

                Assert.AreEqual(testMessage, message.Message);
            }
        }
    }
}
