﻿using LMS.Data;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace LMS.EFTests.BookManagerTests
{
    [TestClass]
    public class AddAuthorToDatabase_Should
    {
        [TestMethod]
        public void AddNewAuthor_ToDatabase_WhenCorrectValuesArePassed()
        {
            var options = TestUtilities.GetOptions(nameof(AddNewAuthor_ToDatabase_WhenCorrectValuesArePassed));
            var bookFactoryMock = new Mock<IBookFactory>();

            Author authorToTest = new Author
            {
                AuthorId = 1,
                Name = "Veso Pesa"
            };

            using (var assertContext = new LMSContext(options))
            {
                var sut = new BookManager(assertContext, bookFactoryMock.Object);
                sut.AddAuthorToDatabase(authorToTest.Name);
                Assert.AreEqual(1, assertContext.Authors.Count());
            }
        }
    }
}
