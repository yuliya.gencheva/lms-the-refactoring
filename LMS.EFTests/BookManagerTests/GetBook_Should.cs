﻿using LMS.Data;
using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace LMS.EFTests.BookManagerTests
{
    [TestClass]
    public class GetBook_Should
    {
        [TestMethod]
        public void ThrowException_WhenBookDoesNotExists()
        {
            Author authorToAdd = new Author { AuthorId = 1, Name = "Sebastian" };

            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenBookDoesNotExists));
            var bookFactoryMock = new Mock<IBookFactory>();

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Authors.Add(authorToAdd);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new BookManager(assertContext, bookFactoryMock.Object);

                Assert.ThrowsException<ArgumentNullException>(() => sut.GetBook("Blood", authorToAdd.Name, 1988));
            }
        }

        [TestMethod]
        public void ThrowCorrectMesasge_WhenBookDoesNotExists()
        {
            var title = "Blood";
            var year = 1988;
            Author authorToAdd = new Author { AuthorId = 1, Name = "Sebastian" };
            var testMessage = $"No book with title {title}, author {authorToAdd.Name} and year {year} exists in Database!";

            var options = TestUtilities.GetOptions(nameof(ThrowCorrectMesasge_WhenBookDoesNotExists));
            var bookFactoryMock = new Mock<IBookFactory>();

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Authors.Add(authorToAdd);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new BookManager(assertContext, bookFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentNullException>(() => sut.GetBook(title, authorToAdd.Name, year));
                Assert.AreEqual(testMessage, ex.Message);
            }
        }

        //I can't understand why it is not working
        //diff checker says that they are identical.
        [TestMethod]
        public void ReturnCorrectBook_WhenBookExists()
        {
            Author authorToAdd = new Author { AuthorId = 3, Name = "Desa Poetesa" };

            var options = TestUtilities.GetOptions(nameof(ReturnCorrectBook_WhenBookExists));
            var bookFactoryMock = new Mock<IBookFactory>();
            var bookToCheck = new Book
            {
                Title = "Blood",
                AuthorId = 3,
                Pages = 123,
                Year = 1988,
                Country = "Bulgaria",
                Language = "Bulgarian",
                Link = "http",
                ISBN = "973-987",
                Category = CategoryType.Detective
            };

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Books.Add(bookToCheck);
                arrangeContext.Authors.Add(authorToAdd);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new BookManager(assertContext, bookFactoryMock.Object);

                Assert.AreEqual(bookToCheck.Title, sut.GetBook(bookToCheck.Title, authorToAdd.Name, bookToCheck.Year).Title);
                Assert.AreEqual(bookToCheck.Author.Name, sut.GetBook(bookToCheck.Title, authorToAdd.Name, bookToCheck.Year).Author.Name);
                Assert.AreEqual(bookToCheck.Pages, sut.GetBook(bookToCheck.Title, authorToAdd.Name, bookToCheck.Year).Pages);
                Assert.AreEqual(bookToCheck.Year, sut.GetBook(bookToCheck.Title, authorToAdd.Name, bookToCheck.Year).Year);
                Assert.AreEqual(bookToCheck.Country, sut.GetBook(bookToCheck.Title, authorToAdd.Name, bookToCheck.Year).Country);
                Assert.AreEqual(bookToCheck.Language, sut.GetBook(bookToCheck.Title, authorToAdd.Name, bookToCheck.Year).Language);
                Assert.AreEqual(bookToCheck.Link, sut.GetBook(bookToCheck.Title, authorToAdd.Name, bookToCheck.Year).Link);
            }
        }
    }
}
