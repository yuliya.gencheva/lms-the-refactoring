﻿using LMS.Data;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.EFTests.LibararyManagerTests
{
    [TestClass]
    public class CheckOutBookShould
    {
        Mock<IBookItemManager> bookItemManagerMock;
        Mock<IUserManager> userManagerMock;
        Mock<IFineManager> fineManagerMock;
        Mock<IReservationManager> reservationManagerMock;
        Mock<IBookIssuedFactory> bookIssuedFactoryMock;


        public CheckOutBookShould()
        {
            this.bookItemManagerMock = new Mock<IBookItemManager>();
            this.userManagerMock = new Mock<IUserManager>();
            this.fineManagerMock = new Mock<IFineManager>();
            this.reservationManagerMock = new Mock<IReservationManager>();
            this.bookIssuedFactoryMock = new Mock<IBookIssuedFactory>();
        }

        [TestMethod]
        public void ThrowException_WhenBookIsReferenceOnly()
        {

            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem { BookItemId = bookItemId, ReferenceOnly = true };
            var newUser = new User { Id = 5, LibraryCardUserName = userName };
            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenBookIsReferenceOnly));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                    reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));

            }
        }

        [TestMethod]
        public void ThrowCorrectMessage_WhenBookIsReferenceOnly()
        {

            int bookItemId = 15;
            string userName = "pesho";
            var message = "Book is reference only!";
            var newBookItem = new BookItem { BookItemId = bookItemId, ReferenceOnly = true };
            var newUser = new User { Id = 5, LibraryCardUserName = userName };
            var options = TestUtilities.GetOptions(nameof(ThrowCorrectMessage_WhenBookIsReferenceOnly));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));
                Assert.AreEqual(message, ex.Message);
            }
        }

        [TestMethod]
        public void ThrowException_WhenBookIsLoaned()
        {

            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem { BookItemId = bookItemId, ReferenceOnly = false, BookStatus = Data.Common.BookStatus.Loaned };
            var newUser = new User { Id = 5, LibraryCardUserName = userName };
            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenBookIsLoaned));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));

            }
        }

        [TestMethod]
        public void ThrowCorrectMessage_WhenBookIsLoaned()
        {

            int bookItemId = 15;
            string userName = "pesho";
            var message = "The book is not available";
            var newBookItem = new BookItem { BookItemId = bookItemId, ReferenceOnly = false, BookStatus = Data.Common.BookStatus.Loaned };
            var newUser = new User { Id = 5, LibraryCardUserName = userName };

            var options = TestUtilities.GetOptions(nameof(ThrowCorrectMessage_WhenBookIsLoaned));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                      reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));
                Assert.AreEqual(message, ex.Message);
            }
        }

        [TestMethod]
        public void ThrowException_WhenUserIsLibrarian()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Available
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 1
            };

            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenUserIsLibrarian));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                      reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));

            }
        }

        [TestMethod]
        public void ThrowCorrectMessage_WhenUserIsLibrarian()
        {

            int bookItemId = 15;
            string userName = "pesho";
            string message = "Libararians cannot check-out books";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Available
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 1
            };

            var options = TestUtilities.GetOptions(nameof(ThrowCorrectMessage_WhenUserIsLibrarian));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));
                Assert.IsTrue(ex.Message.Contains(message));
            }
        }

        [TestMethod]
        public void ThrowException_WhenUserHasFines()
        {

            int bookItemId = 15;
            string userName = "pesho";
            var fine = new Fine { UserId = 5 };
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Available
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine> { fine },
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenUserHasFines));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> { fine });
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.Fines.Add(fine);
                arrangeContext.SaveChanges();

            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                      reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));

            }
        }

        [TestMethod]
        public void ThrowCorrectMessage_WhenUserHasFines()
        {

            int bookItemId = 15;
            string userName = "pesho";
            var fine = new Fine { UserId = 5 };
            var message = "You have pending fines:";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Available
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine> { fine },
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(ThrowCorrectMessage_WhenUserHasFines));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> { fine });
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.Fines.Add(fine);
                arrangeContext.SaveChanges();

            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));
                Assert.IsTrue(ex.Message.Contains(message));
            }
        }



        [TestMethod]
        public void ThrowException_WhenUserHasIssuedMoreThan5Books()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Available
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenUserHasIssuedMoreThan5Books));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> { });

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.BooksIssued.AddRange(new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 },
                    new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                      reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));

            }
        }
        [TestMethod]
        public void ThrowCorrectExceptionMessage_WhenUserHasIssuedMoreThan5Books()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var message = "Exceeded maximum number of books!";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Available
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(ThrowCorrectExceptionMessage_WhenUserHasIssuedMoreThan5Books));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> { });

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.BooksIssued.AddRange(new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 },
                    new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                      reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                var ex = Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));
                Assert.IsTrue(ex.Message.Contains(message));
            }
        }
        [TestMethod]
        public void CallBookIssuedFactory_WithCorrectParams()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Available
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(CallBookIssuedFactory_WithCorrectParams));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> { });

            bookIssuedFactoryMock.Setup(b => b.CreateBookIssued(newBookItem, newUser)).Returns(new BookIssued());
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.BooksIssued.AddRange(new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 }); ;
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                sut.CheckOutBook(bookItemId, userName);
                bookIssuedFactoryMock.Verify(b => b.CreateBookIssued(newBookItem, newUser), Times.Once);
            }
        }

        [TestMethod]
        public void AddBookIssuedToDB()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Available
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(AddBookIssuedToDB));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> {});

            bookIssuedFactoryMock.Setup(b => b.CreateBookIssued(newBookItem, newUser)).Returns(new BookIssued());
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.BooksIssued.AddRange(new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 }); ;
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                sut.CheckOutBook(bookItemId, userName);
                Assert.AreEqual(1, assertContext.BooksIssued.Local.Count);
            }
        }

        [TestMethod]
        public void CallBookItemManager_ModifyBookItem_WithCorrectParams()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Available
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(CallBookItemManager_ModifyBookItem_WithCorrectParams));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            bookItemManagerMock.Setup(bim => bim.ModifyBookItem(bookItemId, "bookstatus", "loaned")).Verifiable();
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> { });

            bookIssuedFactoryMock.Setup(b => b.CreateBookIssued(newBookItem, newUser)).Returns(new BookIssued());
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.BooksIssued.AddRange(new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 }); ;
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                sut.CheckOutBook(bookItemId, userName);
                bookItemManagerMock.Verify(b => b.ModifyBookItem(bookItemId, "bookstatus", "loaned"), Times.Once());
                //Assert.AreEqual(Data.Common.BookStatus.Loaned, newBookItem.BookStatus);
            }
        }

        [TestMethod]
        public void CallBookIssuedFactory_WhenBookIsReservedForUser()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Reserved
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(CallBookIssuedFactory_WhenBookIsReservedForUser));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            reservationManagerMock.Setup(rm => rm.GetReservation(bookItemId))
                .Returns(new BookReservation { User = newUser, BookItem = newBookItem });
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> { });

            bookIssuedFactoryMock.Setup(b => b.CreateBookIssued(newBookItem, newUser)).Returns(new BookIssued());
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                //arrangeContext.BookReservations.Add(new BookReservation { User = newUser, BookItem = newBookItem });
                arrangeContext.BooksIssued.AddRange(new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 }); ;
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                sut.CheckOutBook(bookItemId, userName);
                bookIssuedFactoryMock.Verify(b => b.CreateBookIssued(newBookItem, newUser), Times.Once);
            }
        }

        [TestMethod]
        public void ThrowException_WhenBookIsReservedForOtherUser()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Reserved
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenBookIsReservedForOtherUser));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> { });

            bookIssuedFactoryMock.Setup(b => b.CreateBookIssued(newBookItem, newUser)).Returns(new BookIssued());
            reservationManagerMock.Setup(rm => rm.GetReservation(bookItemId))
                .Returns(new BookReservation { User = new User { Id = 7, LibraryCardUserName = "zzzz" }, BookItem = newBookItem });
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                //arrangeContext.BookReservations.Add(new BookReservation { User = new User { Id = 7, LibraryCardUserName = "zzzz" }, BookItem = newBookItem });
                arrangeContext.BooksIssued.AddRange(new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 }); ;
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                      reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));
            }
        }

        [TestMethod]
        public void ThrowCorrectExceptionMessage_WhenBookIsReservedForOtherUser()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem
            {
                BookItemId = bookItemId,
                ReferenceOnly = false,
                BookStatus = Data.Common.BookStatus.Reserved
            };
            var newUser = new User
            {
                Id = 5,
                LibraryCardUserName = userName,
                Fines = new List<Fine>(),
                RoleId = 2
            };

            var options = TestUtilities.GetOptions(nameof(ThrowCorrectExceptionMessage_WhenBookIsReservedForOtherUser));
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            fineManagerMock.Setup(fm => fm.GetUsersFines(newUser)).Returns(new List<Fine> { });

            bookIssuedFactoryMock.Setup(b => b.CreateBookIssued(newBookItem, newUser)).Returns(new BookIssued());
            reservationManagerMock.Setup(rm => rm.GetReservation(bookItemId))
                .Returns(new BookReservation { User = new User { Id = 7, LibraryCardUserName = "zzzz" }, BookItem = newBookItem });
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.Users.Add(newUser);
                arrangeContext.BookItems.Add(newBookItem);
                //arrangeContext.BookReservations.Add(new BookReservation { User = new User { Id = 7, LibraryCardUserName = "zzzz" }, BookItem = newBookItem });
                arrangeContext.BooksIssued.AddRange(new BookIssued { UserId = 5 }, new BookIssued { UserId = 5 }); ;
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                      reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentException>(() => sut.CheckOutBook(bookItemId, userName));
                Assert.AreEqual("The book is not available", ex.Message);
            }
        }
    }
}