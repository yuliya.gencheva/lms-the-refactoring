﻿using LMS.Data;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.EFTests.RentalManagerTests
{
    [TestClass]
    public class RenewBookShould
    {
        Mock<IBookItemManager> bookItemManagerMock;
        Mock<IUserManager> userManagerMock;
        Mock<IFineManager> fineManagerMock;
        Mock<IReservationManager> reservationManagerMock;
        Mock<IBookIssuedFactory> bookIssuedFactoryMock;


        public RenewBookShould()
        {
            this.bookItemManagerMock = new Mock<IBookItemManager>();
            this.userManagerMock = new Mock<IUserManager>();
            this.fineManagerMock = new Mock<IFineManager>();
            this.reservationManagerMock = new Mock<IReservationManager>();
            this.bookIssuedFactoryMock = new Mock<IBookIssuedFactory>();
        }

        [TestMethod]
        public void ThrowException_WhenUserIsNotRentingTheBook() {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem { BookItemId = bookItemId};
            var newWrongItem = new BookItem { BookItemId = 17};
            var newUser = new User { Id = 5, LibraryCardUserName = userName };
            var wrongUser = new User { Id = 6, LibraryCardUserName = "gosho" };
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenUserIsNotRentingTheBook));
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.BooksIssued.Add(new BookIssued {BookItem=newBookItem, User=wrongUser });
                arrangeContext.SaveChanges();

            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                         reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                Assert.ThrowsException<ArgumentException>(() => sut.RenewBook(bookItemId, userName));
            }


        }

        [TestMethod]
        public void ThrowCorrectExceptionMessage_WhenUserIsNotRentingTheBook()
        {

            var message = $"The bookItem with Id 15 is not borrowed by user pesho!";
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem { BookItemId = bookItemId };
            var newWrongItem = new BookItem { BookItemId = 17 };
            var newUser = new User { Id = 5, LibraryCardUserName = userName };
            var wrongUser = new User { Id = 6, LibraryCardUserName = "gosho" };
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            var options = TestUtilities.GetOptions(nameof(ThrowCorrectExceptionMessage_WhenUserIsNotRentingTheBook));
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.BooksIssued.Add(new BookIssued { BookItem = newBookItem, User = wrongUser });
                arrangeContext.SaveChanges();

            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                         reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                var ex=Assert.ThrowsException<ArgumentException>(() => sut.RenewBook(bookItemId, userName));
                Assert.AreEqual(message, ex.Message);
            }
        }

        [TestMethod]
        public void ThrowException_WhenBookIssuedDoesNotExist()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem { BookItemId = bookItemId };
            var newWrongItem = new BookItem { BookItemId = 17 };
            var newUser = new User { Id = 5, LibraryCardUserName = userName };
            var wrongUser = new User { Id = 6, LibraryCardUserName = "gosho" };
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenBookIssuedDoesNotExist));

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.BooksIssued.Add(new BookIssued() { BookItemId = 16 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                Assert.ThrowsException<ArgumentNullException>(() => sut.RenewBook(bookItemId, userName));
            }
        }

        [TestMethod]
        public void ThrowCorrectExceptionMessage_WhenBookIssuedDoesNotExist()
        {
            int bookItemId = 15;
            string userName = "pesho";
            var newBookItem = new BookItem { BookItemId = bookItemId };
            var newWrongItem = new BookItem { BookItemId = 17 };
            var newUser = new User { Id = 5, LibraryCardUserName = userName };
            var wrongUser = new User { Id = 6, LibraryCardUserName = "gosho" };
            bookItemManagerMock.Setup(bim => bim.GetBookItem(bookItemId)).Returns(newBookItem);
            userManagerMock.Setup(um => um.GetUser(userName)).Returns(newUser);
            var options = TestUtilities.GetOptions(nameof(ThrowCorrectExceptionMessage_WhenBookIssuedDoesNotExist));

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BookItems.Add(newBookItem);
                arrangeContext.BooksIssued.Add(new BookIssued() { BookItemId = 16 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                      reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentNullException>(() => sut.RenewBook(bookItemId,userName ));

                Assert.AreEqual($"BookItem with Id {bookItemId} is not issued!", ex.Message);
            }
        }
    }
}
