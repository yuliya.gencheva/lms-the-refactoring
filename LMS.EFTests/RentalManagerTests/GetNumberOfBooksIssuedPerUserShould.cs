﻿using LMS.Data;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.EFTests.RentalManagerTests
{
    [TestClass]
    public class GetNumberOfBooksIssuedPerUserShould
    {
        Mock<IBookItemManager> bookItemManagerMock;
        Mock<IUserManager> userManagerMock;
        Mock<IFineManager> fineManagerMock;
        Mock<IReservationManager> reservationManagerMock;
        Mock<IBookIssuedFactory> bookIssuedFactoryMock;

        
        public GetNumberOfBooksIssuedPerUserShould()
        {
            this.bookItemManagerMock = new Mock<IBookItemManager>();
            this.userManagerMock = new Mock<IUserManager>();
            this.fineManagerMock = new Mock<IFineManager>();
            this.reservationManagerMock = new Mock<IReservationManager>();
            this.bookIssuedFactoryMock = new Mock<IBookIssuedFactory>();
        }

        [TestMethod]
        public void ReturnCorrectNumberOfBooksIssued()
        {

            int bookItemId = 15;
            int bookItemId2 = 16;
            var user = new User { Id = 6, LibraryCardUserName = "pesho" };

            var options = TestUtilities.GetOptions(nameof(ReturnCorrectNumberOfBooksIssued));

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BooksIssued.Add(new BookIssued()
                {
                    BookItemId = bookItemId,
                    User = user
                });

                arrangeContext.BooksIssued.Add(new BookIssued()
                {
                    BookItemId = bookItemId2,
                    User = user
                });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                        reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                var count = sut.GetNumberOfBooksIssuedPerUser(user);
                Assert.AreEqual(2, count);
            }
        }

        [TestMethod]
        public void ThrowException_WhenPassedUserIsNull()
        {

            int bookItemId = 15;
            int bookItemId2 = 16;
            User user = null;

            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenPassedUserIsNull));

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                        reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                Assert.ThrowsException<ArgumentNullException>(() => sut.GetNumberOfBooksIssuedPerUser(user));
            }
        }

        [TestMethod]
        public void ThrowExceptionWithCorrectMesssage_WhenPassedUserIsNull()
        {

            int bookItemId = 15;
            int bookItemId2 = 16;
            var message = "Invalid user!";
            User user = null;

            var options = TestUtilities.GetOptions(nameof(ThrowExceptionWithCorrectMesssage_WhenPassedUserIsNull));

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                        reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentNullException>(() => sut.GetNumberOfBooksIssuedPerUser(user));
                Assert.AreEqual(message, ex.Message);
            }
        }
    }
}

