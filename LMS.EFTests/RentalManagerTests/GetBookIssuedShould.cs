﻿using LMS.Data;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.EFTests.LibararyManagerTests
{
    [TestClass]
    public class GetBookIssuedShould
    {

        Mock<IBookItemManager> bookItemManagerMock;
        Mock<IUserManager> userManagerMock;
        Mock<IFineManager> fineManagerMock;
        Mock<IReservationManager> reservationManagerMock;
        Mock<IBookIssuedFactory> bookIssuedFactoryMock;


        public GetBookIssuedShould()
        {
            this.bookItemManagerMock = new Mock<IBookItemManager>();
            this.userManagerMock = new Mock<IUserManager>();
            this.fineManagerMock = new Mock<IFineManager>();
            this.reservationManagerMock = new Mock<IReservationManager>();
            this.bookIssuedFactoryMock = new Mock<IBookIssuedFactory>();
        }

        [TestMethod]
        public void GetCorrectBookIssued()
        {
            int bookItemId = 15;
            var newUser = new User { Id = 5, LibraryCardUserName = "pesho" };
            var options = TestUtilities.GetOptions(nameof(GetCorrectBookIssued));
            

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BooksIssued.Add(new BookIssued() { BookItemId = bookItemId, User= newUser });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);
                var newBookIssued = sut.GetBookIssued(bookItemId);
                Assert.AreEqual(bookItemId, newBookIssued.BookItemId);
            }

        }

        [TestMethod]
        public void ThrowException_WhenBookIssuedDoesNotExist()
        {
            int bookItemId = 15;
            var options = TestUtilities.GetOptions(nameof(ThrowException_WhenBookIssuedDoesNotExist));

            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BooksIssued.Add(new BookIssued() { BookItemId = 16 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                     reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                Assert.ThrowsException<ArgumentNullException>(() => sut.GetBookIssued(bookItemId));
            }
        }

        [TestMethod]
        public void ThrowCorrectExceptionMessage_WhenBookIssuedDoesNotExist()
        {
            int bookItemId = 15;
            var options = TestUtilities.GetOptions(nameof(ThrowCorrectExceptionMessage_WhenBookIssuedDoesNotExist));
           
            using (var arrangeContext = new LMSContext(options))
            {
                arrangeContext.BooksIssued.Add(new BookIssued() { BookItemId = 16 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSContext(options))
            {
                var sut = new RentalManager(assertContext, bookItemManagerMock.Object, userManagerMock.Object,
                      reservationManagerMock.Object, fineManagerMock.Object, bookIssuedFactoryMock.Object);

                var ex = Assert.ThrowsException<ArgumentNullException>(() => sut.GetBookIssued(bookItemId));

                Assert.AreEqual($"BookItem with Id {bookItemId} is not issued!", ex.Message);
            }
        }
    }
}