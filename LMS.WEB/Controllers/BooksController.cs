﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LMS.Data;
using LMS.Data.Models;
using LMS.Services;
using LMS.Services.Contracts;
using LMS.Data.Common;
using LMS.WEB.ViewModels;

namespace LMS.WEB.Controllers
{
    public class BooksController : Controller
    {
        private readonly LMSContext _context;

        private IBookManager BookManager;

        public BooksController(IBookManager bookManager, LMSContext context)
        {
            this.BookManager = bookManager;
            _context = context;
        }

        // GET: Books
        public async Task<IActionResult> Index()
        {
           var allBooks = await this.BookManager.GetAllBooks();
            allBooks = allBooks.OrderBy(b => b.Title).ThenBy(b => b.Author.Name);
            return View(allBooks);
        }

        // GET: Books/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            var book = await this.BookManager.GetBook(id);
            var bookViewModel = new BookViewModel(book);
            return View(bookViewModel);
        }

        // GET: Books/Create
        public IActionResult Create()
        { 

            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BookViewModel modelView)
            
            //[Bind("BookId,Title,AuthorId,Pages,Year,Country,Language,Link,ISBN,Category,IsDeleted")] Book book)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invlid book parameters");
            }

            await this.BookManager.AddBook(modelView.Title, modelView.AuthorName, modelView.Pages, modelView.Year, modelView.Country, modelView.Language, modelView.Link,
                modelView.ImageURL, modelView.ISBN, modelView.Category);
            return RedirectToAction(nameof(Index));
        }

        // GET: Books/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await this.BookManager.GetBook(id);
            if (book == null)
            {
                return NotFound();
            }
            ViewData["AuthorId"] = new SelectList(_context.Authors, "AuthorId", "Name", book.AuthorId);
            return View(book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BookId,Title,AuthorId,Pages,Year,Country,Language,Link,ImageURL,ISBN,Category,IsDeleted")] Book book)
        {
            if (id != book.BookId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await this.BookManager.EditBook(book);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.BookId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["AuthorId"] = new SelectList(_context.Authors, "AuthorId", "Name", book.AuthorId);
            return View(book);
        }

        // GET: Books/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.Author)
                .FirstOrDefaultAsync(m => m.BookId == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var book = await _context.Books.FindAsync(id);
            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookExists(int? id)
        {
            bool isFound = false;
            if (this.BookManager.GetBook(id) == null)
                isFound = true;
            return  isFound;
        }
    }
}
