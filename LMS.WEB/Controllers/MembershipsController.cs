﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LMS.Data;
using LMS.Data.Models;
using LMS.WEB.ViewModels;
using LMS.Services;
using System.Security.Claims;
using LMS.Services.Contracts;
using Microsoft.AspNetCore.Identity;

namespace LMS.WEB.Controllers
{
    public class MembershipsController : Controller
    {
        private readonly IMembershipManager manager;
        private readonly UserManager<User> userManager;
        private readonly ILibraryUserManager libraryUserManager;

        public MembershipsController(IMembershipManager manager, UserManager<User> userManager,
            ILibraryUserManager libraryUserManager)
        {
            this.manager = manager;
            this.userManager = userManager;
            this.libraryUserManager = libraryUserManager;
        }

        //// GET: Memberships
        //public async Task<IActionResult> Index()
        //{
        //    return View(await _context.Memberships.ToListAsync());
        //}

        //// GET: Memberships/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var membership = await .Memberships
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (membership == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(membership);
        //}

        // GET: Memberships/Create
        public IActionResult Create()
        {
            var viewModel = new MembershipViewModel()
            { UserId=this.User.FindFirstValue(ClaimTypes.NameIdentifier)};
            return View(viewModel);
        }

        // POST: Memberships/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MembershipViewModel membershipViewModel)
        {
            if (!ModelState.IsValid)
            {
                throw new ArgumentException("Invalid P");

            }

            var membership=await this.manager.AddMembership(membershipViewModel.UserId, membershipViewModel.Term);
            var newUser = await this.libraryUserManager.GetUser(membershipViewModel.UserId);
            await this.userManager.AddToRoleAsync(newUser, "Member");
            TempData["Success"] = $"Welcome on board! Your membership is valid until {membership.Validity.Date}!";
            return RedirectToAction("Index", "Home");
        }

        //// GET: Memberships/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var membership = await _context.Memberships.FindAsync(id);
        //    if (membership == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(membership);
        //}

        //// POST: Memberships/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,Created,Validity,UserId")] Membership membership)
        //{
        //    if (id != membership.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(membership);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!MembershipExists(membership.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(membership);
        //}

        //// GET: Memberships/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var membership = await _context.Memberships
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (membership == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(membership);
        //}

        //// POST: Memberships/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var membership = await _context.Memberships.FindAsync(id);
        //    _context.Memberships.Remove(membership);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool MembershipExists(int id)
        //{
        //    return _context.Memberships.Any(e => e.Id == id);
        //}
    }
}
