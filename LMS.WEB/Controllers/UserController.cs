﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Data.Models;
using LMS.Services.Contracts;
using LMS.WEB.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LMS.WEB.Controllers
{
    public class UserController : Controller
    {
        private readonly ILibraryUserManager libraryUserManager;
        private readonly UserManager<User> userManager;

        public UserController(ILibraryUserManager libraryUserManager, UserManager<User> userManager)
        {
            this.libraryUserManager = libraryUserManager;
            this.userManager = userManager;
        }

        [Authorize(Roles = "Administrator, Librarian")]
        public async Task<IActionResult> ListUsers()
        {
            var users = await this.libraryUserManager.GetAllUsers();
            List<UserViewModel> usersViewModels = new List<UserViewModel>();
            foreach (var user in users)
            {
                var roles = await this.userManager.GetRolesAsync(user);
                string role;
                if (roles.Count == 0)
                {
                    role = "No role";
                }
                else
                {
                    role = roles[0];
                }
                usersViewModels.Add(new UserViewModel(user, role));
            }

            return View(usersViewModels);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [Route(nameof(ChangeRoleToLibrarian) + "/{userId}")]
        public async Task<IActionResult> ChangeRoleToLibrarian(string userId)
        {
            var user = await this.libraryUserManager.GetUser(userId);
            if (!await this.userManager.IsInRoleAsync(user, "Librarian"))
            {
                await this.userManager.AddToRoleAsync(user, "Librarian");
            }
            return RedirectToAction("ListUsers", "User");
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, Librarian")]
        [Route(nameof(BanUser) + "/{userId}")]
        public async Task<IActionResult> BanUser(string userId)
        {
            var user = await this.libraryUserManager.GetUser(userId);
            await this.libraryUserManager.BanUser(userId);
            return RedirectToAction("ListUsers", "User");
        }
    }
}