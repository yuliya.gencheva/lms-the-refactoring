﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.WEB.ViewModels
{
    public class MembershipViewModel
    {
        public int Term { get; set; }
        public string UserId { get; set; }
    }
}
