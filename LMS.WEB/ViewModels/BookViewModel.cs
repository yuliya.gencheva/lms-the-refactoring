﻿using LMS.Data.Common;
using LMS.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.WEB.ViewModels
{
    public class BookViewModel
    {
        public BookViewModel()
        {

        }
        public BookViewModel(Book book)
        {
            Id = book.BookId;
            Title = book.Title;
            AuthorName = book.Author.Name;
            Pages = book.Pages;
            Year = book.Year;
            Country = book.Country;
            Language = book.Language;
            Link = book.Link;
            ImageURL = book.ImageURL;
            ISBN = book.ISBN;
            IsDeleted = book.IsDeleted;
            Category = book.Category;
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required!")]
        [MinLength(2, ErrorMessage = "Title must be between 3 and 30 symbols"),
            MaxLength(50, ErrorMessage = "Title must be between 3 and 30 symbols")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Author's name is required!")]
        [MinLength(3, ErrorMessage = "Author's name must be between 3 and 30 symbols"),
            MaxLength(30, ErrorMessage = "Author's name must be between 3 and 30 symbols")]
        public string AuthorName { get; set; }

        [Required(ErrorMessage = "Pages are required!")]
        [Range(0, 10000, ErrorMessage = "Pages must be between 0 and 10 000!")]
        public int Pages { get; set; }

        [Required(ErrorMessage = "Published year is required!")]
        public int Year { get; set; }

        [Required(ErrorMessage = "Country of origin is required!")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Language is required!")]
        public string Language { get; set; }

        [Required(ErrorMessage = "A link to a description is required!")]
        public string Link { get; set; }

        [Required(ErrorMessage = "A link to an image is required!")]
        public string ImageURL { get; set; }

        [Required(ErrorMessage = "A unique ISBN is required!")]
        public string ISBN { get; set; }

        public CategoryType Category { get; set; }

        public bool IsDeleted { get; set; }
    }
}
