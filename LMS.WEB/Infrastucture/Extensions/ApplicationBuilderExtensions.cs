﻿namespace Instagreat.Web.Infrastructure.Extensions
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using LMS.Data;
    using System.Threading.Tasks;
    using LMS.Data.Models;

    public static class ApplicationBuilderExtensions
    {
        public static void UpdateDatabase(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<LMSContext>();
                context.Database.Migrate();

                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>();
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<User>>();

                Task.Run(async () =>
                {
                    var adminName = "Administrator";

                    var exists = await roleManager.RoleExistsAsync(adminName);

                    if (!exists)
                    {
                        await roleManager.CreateAsync(new IdentityRole
                        {
                            Name = adminName
                        });
                    }

                    var librarianName = "Librarian";

                    var existsLibrarian = await roleManager.RoleExistsAsync(librarianName);

                    if (!existsLibrarian)
                    {
                        await roleManager.CreateAsync(new IdentityRole
                        {
                            Name = librarianName
                        });
                    }

                    var memberName = "Member";

                    var existsMember = await roleManager.RoleExistsAsync(memberName);

                    if (!existsMember)
                    {
                        await roleManager.CreateAsync(new IdentityRole
                        {
                            Name = memberName
                        });
                    }

                    var adminUser = await userManager.FindByNameAsync(adminName);

                    if (adminUser == null)
                    {
                        adminUser = new User
                        {
                            UserName = "admin",
                            Email = "admin@admin.com"
                        };

                        await userManager.CreateAsync(adminUser, "admin12");
                        await userManager.AddToRoleAsync(adminUser, adminName);
                    }
                })
                .GetAwaiter()
                .GetResult();
            }
        }
    }
}