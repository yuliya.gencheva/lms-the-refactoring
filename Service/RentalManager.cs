﻿using LMS.Data;
using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Services
{
    public class RentalManager : IRentalManager
    {
        private readonly LMSContext context;
        private readonly IBookItemManager bookItemManager;
        private readonly ILibraryUserManager userManager;
        private readonly IReservationManager reservationManager;
        private readonly IBookIssuedFactory bookIssuedFactory;



        public RentalManager(LMSContext context, IBookItemManager bookItemManager, ILibraryUserManager userManager,
            IReservationManager reservationManager, IBookIssuedFactory bookIssuedFactory)
        {
            this.context = context;
            this.bookItemManager = bookItemManager;
            this.userManager = userManager;
            this.reservationManager = reservationManager;
            this.bookIssuedFactory = bookIssuedFactory;
        }

        public async Task<BookIssued> GetBookIssued(int? bookItemId)
        {
            var bookIssued = await context.BooksIssued.Include(bi => bi.User).SingleOrDefaultAsync(bi => bi.BookItemId == bookItemId);
            bookIssued.ValidateIfNull($"BookItem with Id {bookItemId} is not issued!");
            return bookIssued;
        }
        public int GetNumberOfBooksIssuedPerUser(User user)
        {
            //user.ValidateIfNull("Invalid user!");

            return context.BooksIssued.Where(b => b.User.UserName == user.UserName).Count();
        }

        public void CheckOutBook(int bookItemId, string userName)
        {
            BookItem bookToCheckOut = bookItemManager.GetBookItem(bookItemId);
            //User userToGetBook = userManager.GetUser(userName);
            //if (bookToCheckOut.ReferenceOnly)
            //{
            //    throw new ArgumentException("Book is reference only!");
            //}
            //if (bookToCheckOut.BookStatus == BookStatus.Loaned)
            //{
            //    throw new ArgumentException("The book is not available");
            //}
            //if (userToGetBook.RoleId == 1)
            //{
            //    throw new ArgumentException("Libararians cannot check-out books");
            //}



            //    if (GetNumberOfBooksIssuedPerUser(userToGetBook) >= 5)
            //    {
            //        throw new ArgumentException("Exceeded maximum number of books!");
            //    }
            //    if (bookToCheckOut.BookStatus == BookStatus.Reserved)
            //    {
            //        var reservation = this.reservationManager.GetReservation(bookToCheckOut.BookItemId);
            //        if (reservation.User.UserName != userToGetBook.UserName)
            //        {
            //            throw new ArgumentException("The book is not available");
            //        }
            //        this.reservationManager.RemoveReservation(reservation);
            //    }

            //    BookIssued bookIssued = this.bookIssuedFactory.CreateBookIssued(bookToCheckOut, userToGetBook);
            //    this.bookItemManager.ModifyBookItem(bookToCheckOut.BookItemId, "bookstatus", "loaned");
            //    context.BooksIssued.Add(bookIssued);
            //    context.SaveChanges();
            //
        }

        public async Task RenewBook(int bookItemId, string userName)
        {
            BookItem bookToRenew = bookItemManager.GetBookItem(bookItemId);
            //User userToRenewBook = userManager.GetUser(userName);
            var bookIssued = await GetBookIssued(bookToRenew.BookItemId);

            //if (bookIssued.User.UserName != userToRenewBook.UserName)
            //{
            //    throw new ArgumentException($"The bookItem with Id {bookToRenew.BookItemId} is not borrowed by user {userToRenewBook.UserName}!");
            //}
            bookIssued.DateOfIssue = DateTime.Now;
            context.SaveChanges();
        }

        public async Task ReturnBook(int bookItemId, string userName)
        {
            BookItem bookToReturn = bookItemManager.GetBookItem(bookItemId);
            //User userToReturnBook = userManager.GetUser(userName);
            var bookIssued = await GetBookIssued(bookToReturn.BookItemId);

            //if (bookIssued.User.UserName != userToReturnBook.UserName)
            //{
            //    throw new ArgumentException($"The bookItem with Id {bookToReturn.BookItemId} is not borrowed by user {userToReturnBook.UserName}!");
            //}
            context.BooksIssued.Remove(bookIssued);

            var bookReservation = context.BookReservations.FirstOrDefault(br => br.BookItemId == bookItemId);
            if (bookReservation != null)
            {
                this.bookItemManager.ModifyBookItem(bookToReturn.BookItemId, "bookstatus", "reserved");
            }
            else
            {
                this.bookItemManager.ModifyBookItem(bookToReturn.BookItemId, "bookstatus", "available");
            }
            context.SaveChanges();
        }

        public async Task<List<BookIssued>> AllBooksIssued(string userId)
        {
            var listOFBooksIssued = await context.BooksIssued.Include(b => b.BookItem).Include(b => b.User).Where(b => b.UserId == userId).ToListAsync();
            return listOFBooksIssued;
        }
    }
}
