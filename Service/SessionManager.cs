﻿using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Services
{
    public class SessionManager : ISessionManager
    {
        private readonly string name = "Trinity College Dublin Library";
        private readonly string address = "Ireland";
        private User loggedUser = null;
        private ILibraryUserManager userManager;

        public SessionManager(ILibraryUserManager userManager)
        {
            this.userManager = userManager;
        }

        public User LoggedUser
        {
            get
            {
                return loggedUser;
            }
        }

        public void ValidateUserAuthority(User user)
        {
            //if (user.RoleId == 1)
            //{
            //    throw new Exception("Libararians cannot check-out books");
            //}
        }

        public string LogIn(string userName)
        {
            var user = userManager.GetUser(userName);
            user.ValidateIfNull($"User with {userName} doesn't exist");
            //loggedUser = user;
            return $"Hello," ;
        }

        public string LogOut()
        {
            LoggedUser.ValidateIfNull("No logged user!");

            loggedUser = null;
            return "Succesfully logged out!";
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Library: {name}");
            sb.AppendLine($"Address: {address}");
            return sb.ToString().Trim();
        }

    }
}
