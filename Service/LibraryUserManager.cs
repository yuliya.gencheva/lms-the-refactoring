﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Data;
using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LMS.Services
{
    public class LibraryUserManager : ILibraryUserManager
    {
        private readonly LMSContext context;
        private readonly IUserFactory userFactory;
        public LibraryUserManager(LMSContext context, IUserFactory userFactory)
        {
            this.context = context;
            this.userFactory = userFactory;
        }

        public void AddNewUser(string name, string userName, int roleId)
        {
            User newMember = userFactory.CreateUser(name, userName, roleId);
            if (context.Users.Any(u => u.UserName == newMember.UserName))
            {
                throw new ArgumentException("This username already exist!");
            }
            context.Users.Add(newMember);
            context.SaveChanges();
        }

        public async Task<User> GetUser(string id)
        {
            var user = await this.context.Users.Where(u =>u.Id == id && u.IsDeleted==false).SingleOrDefaultAsync();
            user.ValidateIfNull($"No active user with id: {id} exists in Database!");
            return user;
        }

        public async Task<List<User>> GetAllUsers()
        {
            var users = await this.context.Users.Where(u => u.IsDeleted==false).ToListAsync();
            return users;
        }

        public async Task BanUser(string userId)
        {
            var user = await this.GetUser(userId);
            user.IsBanned = true;
            await this.context.SaveChangesAsync();
        }

        public void ModifyUser(string userName, string newName)
        {
            //User user = GetUser(userName);
            //context.SaveChanges();
        }

        public void RemoveUser(string userNameToDelete)
        {
            //User userToDelete = GetUser(userNameToDelete);
            //if (context.BooksIssued.Where(bi => bi.UserId == userToDelete.Id).Any())
            //{
            //    throw new ArgumentException("The user must return all books before membership can be cancelled!");
            //}
            //if (context.BookReservations.Where(br => br.UserId == userToDelete.Id).Any())
            //{
            //    throw new ArgumentException("The user must cancel all reservations before membership can be cancelled!");
            //}

            //context.Users.Remove(userToDelete);
            context.SaveChanges();
        }
    }
}
