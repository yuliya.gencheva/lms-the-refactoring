﻿using LMS.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Services.Contracts
{
    public interface IRentalManager
    {
        void CheckOutBook(int bookItemId, string userName);
        Task<BookIssued> GetBookIssued(int? bookItemId);
        int GetNumberOfBooksIssuedPerUser(User user);
        Task RenewBook(int bookItemId, string userName);
        Task ReturnBook(int bookItemId, string userName);
        Task<List<BookIssued>> AllBooksIssued(string userId);
    }
}