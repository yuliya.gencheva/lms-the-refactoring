﻿
using LMS.Data.Common;
using LMS.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Services.Contracts
{
    public interface IBookManager
    {
        Task AddBook(string title, string author, int pages, int year, string country, string language,
            string link, string url, string isbn, CategoryType category);
        Task<Book> GetBook(int? id);
        Task RemoveBook(int? id);
        Task<IEnumerable<Book>> GetAllBooks();
        Task EditBook(Book book);
        List<Book> GetBooksByCriteria(string searchOption, string searchArgs);
    }
}
//void ModifyBook(IBook book);