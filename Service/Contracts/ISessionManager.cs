﻿
using LMS.Data.Models;

namespace LMS.Services.Contracts
{
    public interface ISessionManager
    {
        User LoggedUser { get; }
        string LogIn(string userName);
        string LogOut();
        void ValidateUserAuthority(User user);
    }
}