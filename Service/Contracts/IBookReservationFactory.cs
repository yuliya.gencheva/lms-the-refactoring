﻿
using LMS.Data.Models;

namespace LMS.Services.Contracts
{
    public interface IBookReservationFactory
    {
        BookReservation CreateBookReservation(BookItem bookToReserve, User user);
    }
}