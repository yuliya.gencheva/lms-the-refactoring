﻿using LMS.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Services.Contracts
{
    public interface ILibraryUserManager
    {
        void AddNewUser(string name, string userName, int roleId);
        Task BanUser(string userId);
        Task<User> GetUser(string userId);
        Task<List<User>> GetAllUsers();
        void ModifyUser(string userName, string newName);
        void RemoveUser(string userNameToDelete);
    }
}