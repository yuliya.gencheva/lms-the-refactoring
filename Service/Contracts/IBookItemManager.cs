﻿
using LMS.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Services.Contracts
{
    public interface IBookItemManager
    {
        Task AddBookItem(int? id, int rack);
        BookItem GetBookItem(int id);
        List<BookItem> GetBookItemsFromBook(Book book);
        void ModifyBookItem(int id, string criteria, string newParameter);
        void RemoveBookItem(int bookItemId);
    }
}