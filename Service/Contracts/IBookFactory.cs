﻿using LMS.Data.Common;
using LMS.Data.Models;

namespace LMS.Services.Contracts
{
    public interface IBookFactory
    {
        Book CreateBook(string title, int authorId, int pages, int year, string country, string language, string link, string image, string isbn, CategoryType category);
    }
}