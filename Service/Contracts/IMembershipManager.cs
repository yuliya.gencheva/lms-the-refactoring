﻿using LMS.Data.Models;
using System.Threading.Tasks;

namespace LMS.Services.Contracts
{
    public interface IMembershipManager
    {
        Task<Membership> AddMembership(string userId, int term);
    }
}