﻿using LMS.Data.Models;

namespace LMS.Services.Contracts
{
    public interface IReservationManager
    {
        int GetNumberOfReservationsPerUser(User user);
        BookReservation GetReservation(int bookItemId);
        void ReserveBook(int bookItemId, string userName);

        void RemoveReservation(BookReservation reservation);
    }
}