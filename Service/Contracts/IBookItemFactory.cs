﻿
using LMS.Data.Models;

namespace LMS.Services.Contracts
{
    public interface IBookItemFactory
    {
        BookItem CreateBookItem(Book book, int rack);
    }
}