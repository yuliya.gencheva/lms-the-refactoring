﻿

using LMS.Data.Models;

namespace LMS.Services.Contracts
{
    public interface IBookIssuedFactory
    {
        BookIssued CreateBookIssued(BookItem bookToIssue, User user);
    }
}