﻿using LMS.Data.Models;

namespace LMS.Services.Contracts
{
    public interface IUserFactory
    {
        User CreateUser(string name, string userName, int roleId);
    }
}