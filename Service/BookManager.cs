﻿using LMS.Data;
using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Services
{
    public class BookManager : IBookManager
    {
        private readonly LMSContext context;
        private readonly IBookFactory bookFactory;

        public BookManager(LMSContext context, IBookFactory bookFactory)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.bookFactory = bookFactory ?? throw new ArgumentNullException(nameof(bookFactory));
        }
      
        public async Task AddBook(string title, string author, int pages, int year, string country, string language,
            string link, string url, string isbn, CategoryType category)
        {
            var authorToAdd = context.Authors.SingleOrDefault(u => u.Name == author);

            if (authorToAdd == null)
            {
                authorToAdd = await AddAuthorToDatabase(author);
            }

            Book bookToAdd = bookFactory.CreateBook(title, authorToAdd.AuthorId, pages, year, country, language, link, url, isbn, category);

            var book = context.Books.FirstOrDefault(b => b.AuthorId == bookToAdd.AuthorId && b.Title == bookToAdd.Title
            && b.Year == bookToAdd.Year);

            if (book!= null)
            {
                throw new ArgumentException("Book already exists in the catalogue");
            }

            await context.Books.AddAsync(bookToAdd);
            await context.SaveChangesAsync();
        }
       
        public async Task<Author> AddAuthorToDatabase(string author)
        {
            Author authorToAdd = new Author { Name = author };
            await context.Authors.AddAsync(authorToAdd);
            await context.SaveChangesAsync();
            return authorToAdd;
        }
       
        public async Task RemoveBook(int? id)
        {
            Book bookToRemove = await GetBook(id);
           
            if (await context.BookItems.AnyAsync(b => b.BookID == bookToRemove.BookId))
            {
                throw new ArgumentException("You must delete all copies of the book");
            }
            bookToRemove.IsDeleted = true;
            await context.SaveChangesAsync();
        }
       
        public async Task<Book> GetBook(int? id)
        {
            var book =await context.Books.Include(b => b.Author)
                .Where(b => b.IsDeleted == false)
                .FirstOrDefaultAsync(b=>b.BookId==id);
            book.ValidateIfNull($"No book with id {id} exists in Database!");
            return book;
        }

        public async Task EditBook(Book book)
        {
            var bookToEdit = new Book();
            if(await this.context.Books.AsNoTracking().SingleOrDefaultAsync(b => b.BookId == book.BookId) != null)
            {
                bookToEdit = book;
            }
                
            this.context.Books.Update(bookToEdit);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Book>> GetAllBooks()
        {
            var books = await context.Books.Include(b => b.Author)
                .Where(b=>b.IsDeleted==false)
                .ToListAsync();
            return books;
        }

        public List<Book> GetBooksByCriteria(string searchOption, string searchArgs)
        {
            List<Book> result;
            switch (searchOption)
            {
                case "title":
                    result = context.Books.Include(b => b.Author)
                        .Where(x => x.Title.ToLower().Contains(searchArgs)).ToList();
                    return result;
                case "author":
                    result = context.Books.Include(b => b.Author)
                        .Where(a => a.Author.Name.ToLower().Contains(searchArgs)).ToList();
                    return result;
                case "category":
                    result = context.Books.Include(b => b.Author)
                        .Where(c => c.Category.ToString().ToLower() == searchArgs).ToList();
                    return result;
                case "year":
                    int year = int.Parse(searchArgs);
                    result = context.Books.Include(b => b.Author)
                        .Where(y => y.Year == year).ToList();
                    return result;
                default:
                    throw new ArgumentException("Invalid search criteria");
            }
        }
    }
}

////TODO
//public void ModifyBook(IBook book)
//{
//    throw new NotImplementedException();
//}
