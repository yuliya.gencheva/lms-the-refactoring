﻿//using System.Text;
//using System.Linq;
//using System;
//using System.Collections.Generic;
//using LMS.Data;
//using LMS.Data.Models;
//using LMS.Data.Common;
//using LMS.Services.Contracts;
//using Microsoft.EntityFrameworkCore;

//namespace LMS.Services
//{
//    public class LibraryManager : ILibraryManager
//    {

//        private readonly string name = "Trinity College Dublin Library";
//        private readonly string address = "Ireland";
//        private readonly LMSContext context;
//        private readonly IBookItemManager bookItemManager;
//        private readonly IUserManager userManager;
//        private readonly IFineManager fineManager;
//        private readonly IBookReservationFactory bookReservationFactory;
//        private readonly IBookIssuedFactory bookIssuedFactory;


//        public LibraryManager(LMSContext context, IBookItemManager bookItemManager, IUserManager userManager,
//            IFineManager fineManager,
//            IBookReservationFactory bookReservationFactory, IBookIssuedFactory bookIssuedFactory)
//        {
//            this.context = context;
//            this.bookItemManager = bookItemManager;
//            this.userManager = userManager;
//            this.fineManager = fineManager;
//            this.bookReservationFactory = bookReservationFactory;
//            this.bookIssuedFactory = bookIssuedFactory;
//        }
//        //public BookIssued GetBookIssued(int bookItemId)
//        //{
//        //    var bookIssued = context.BooksIssued.SingleOrDefault(bi => bi.BookItemId == bookItemId);
//        //    bookIssued.ValidateIfNull($"BookItem with Id {bookItemId} is not issued!");
//        //    return bookIssued;
//        //}

//        //public BookReservation GetReservation(int bookItemId)
//        //{
//        //    var bookReservation = context.BookReservations.Include(br=>br.User).FirstOrDefault(br => br.BookItemId == bookItemId);
//        //    bookReservation.ValidateIfNull($"BookItem with Id {bookItemId} is not reserved!");
//        //    return bookReservation;
//        //}
//        //public int GetNumberOfReservationsPerUser(User user)
//        //{
//        //    user.ValidateIfNull("Invalid user!");

//        //    return context.BookReservations.Where(b => b.User.Name == user.Name).Count();
//        //}


//        //public void CheckOutBook(int bookItemId, string userName)
//        //{
//        //    BookItem bookToCheckOut = bookItemManager.GetBookItem(bookItemId);
//        //    User userToGetBook = userManager.GetUser(userName);
//        //    if (bookToCheckOut.ReferenceOnly)
//        //    {
//        //        throw new ArgumentException("Book is reference only!");
//        //    }
//        //    if (bookToCheckOut.BookStatus == BookStatus.Loaned)
//        //    {
//        //        throw new ArgumentException("The book is not available");
//        //    }
//        //    if (userToGetBook.RoleId == 1)
//        //    {
//        //        throw new ArgumentException("Libararians cannot check-out books");
//        //    }

//        //    var listOfFines = this.fineManager.GetUsersFines(userToGetBook);
//        //    if (listOfFines.Count() > 0)
//        //    {
//        //        throw new ArgumentException("You have pending fines: " + Environment.NewLine
//        //            + string.Join(Environment.NewLine, listOfFines));
//        //    }
//        //    if (GetNumberOfBooksIssuedPerUser(userToGetBook) >= 5)
//        //    {
//        //        throw new ArgumentException("Exceeded maximum number of books!");
//        //    }
//        //    if (bookToCheckOut.BookStatus == BookStatus.Reserved)
//        //    {
//        //        var reservation = GetReservation(bookToCheckOut.BookItemId);
//        //        if (reservation.User.UserName != userToGetBook.UserName)
//        //        {
//        //            throw new ArgumentException("The book is not available");
//        //        }
//        //        context.BookReservations.Remove(reservation);
//        //    }

//        //    BookIssued bookIssued = bookIssuedFactory.CreateBookIssued(bookToCheckOut, userToGetBook);
//        //    bookToCheckOut.ChangeBookStatus(BookStatus.Loaned);
//        //    context.BooksIssued.Add(bookIssued);
//        //    context.SaveChanges();
//        //}

//        //public void RenewBook(int bookItemId, string userName)
//        //{
//        //    BookItem bookToRenew = bookItemManager.GetBookItem(bookItemId);
//        //    User userToRenewBook = userManager.GetUser(userName);
//        //    var bookIssued = GetBookIssued(bookToRenew.BookItemId);

//        //    if (bookIssued.User.UserName != userToRenewBook.UserName)
//        //    {
//        //        throw new ArgumentException($"The bookItem with Id {bookToRenew.BookItemId} is not borrowed by user {userToRenewBook.UserName}!");
//        //    }
//        //    this.fineManager.CalculateFine(userToRenewBook, bookIssued);
//        //    bookIssued.DateOfIssue = DateTime.Now;
//        //    context.SaveChanges();
//        //}

//        //public void ReserveBook(int bookItemId, string userName)
//        //{
//        //    BookItem bookToReserve = bookItemManager.GetBookItem(bookItemId);
//        //    User userToReserveBook = userManager.GetUser(userName);
//        //    if (bookToReserve.ReferenceOnly)
//        //    {
//        //        throw new ArgumentException("Book is reference only!");
//        //    }
//        //    if (userToReserveBook.RoleId == 1)
//        //    {
//        //        throw new Exception("Libararians cannot check-out books");
//        //    }
//        //    var listOfFines = this.fineManager.GetUsersFines(userToReserveBook);
//        //    if (listOfFines.Count() > 0)
//        //    {
//        //        throw new ArgumentException("You have pending fines: " + Environment.NewLine
//        //            + string.Join(Environment.NewLine, listOfFines));
//        //    }
            
//        //    if (GetNumberOfReservationsPerUser(userToReserveBook) >= 5)
//        //    {
//        //        throw new ArgumentException("Exceeded maximum number of books!");
//        //    }
//        //    if (bookToReserve.BookStatus == BookStatus.Available)
//        //    {
//        //        bookToReserve.ChangeBookStatus(BookStatus.Reserved);
//        //    }
//        //    BookReservation reservation = bookReservationFactory.CreateBookReservation(bookToReserve, userToReserveBook);
//        //    context.BookReservations.Add(reservation);
//        //    context.SaveChanges();
//        //}

//        //public void ReturnBook(int bookItemId, string userName)
//        //{
//        //    BookItem bookToReturn = bookItemManager.GetBookItem(bookItemId);
//        //    User userToReturnBook = userManager.GetUser(userName);
//        //    var bookIssued = GetBookIssued(bookToReturn.BookItemId);

//        //    if (bookIssued.User.UserName != userToReturnBook.UserName)
//        //    {
//        //        throw new ArgumentException($"The bookItem with Id {bookToReturn.BookItemId} is not borrowed by user {userToReturnBook.UserName}!");
//        //    }
//        //    this.fineManager.CalculateFine(userToReturnBook, bookIssued);
//        //    context.BooksIssued.Remove(bookIssued);

//        //    var bookReservation = context.BookReservations.FirstOrDefault(br => br.BookItemId == bookItemId);
//        //    if (bookReservation != null)
//        //    {
//        //        bookToReturn.ChangeBookStatus(BookStatus.Reserved);
//        //    }
//        //    else
//        //    {
//        //        bookToReturn.ChangeBookStatus(BookStatus.Available);
//        //    }
//        //    context.SaveChanges();
//        //}



//        //public int GetNumberOfBooksIssuedPerUser(User user)
//        //{
//        //    user.ValidateIfNull("Invalid user!");

//        //    return context.BooksIssued.Where(b => b.User.UserName == user.UserName).Count();
//        //}
//        public override string ToString()
//        {
//            StringBuilder sb = new StringBuilder();
//            sb.AppendLine($"Library: {name}");
//            sb.AppendLine($"Address: {address}");
//            return sb.ToString().Trim();
//        }
//    }
//    //private decimal moneyBalance;
//}
////TODO
////public void CollectFines(decimal fine)
////{
////    this.moneyBalance += fine;
////}

////public int CheckForOverdueBooks()
////{
////    int sentNotifications = 0;
////    foreach (var bookIssued in this.bookIssuedDatabase.BooksIssued)
////    {
////        if (bookIssued.DateOfIssue.AddDays(10) > DateTime.Now)
////        {
////            bookIssued.User.AddNotification($"{bookIssued.BookItem.Book.Title} is overdue! You must return or renew the book!");
////            sentNotifications++;
////        }
////    }
////    return sentNotifications;
////}