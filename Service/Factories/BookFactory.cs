﻿using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services.Contracts;
using System;

namespace LMS.Services.Factories
{
    public class BookFactory : IBookFactory
    {
        public Book CreateBook(string title, int authorId, int pages, int year, string country, string language, string link, string image, string isbn, CategoryType category)
        {
            
            if (title.Length < 2 || title.Length > 50)
                throw new ArgumentOutOfRangeException("Title should be between 2 and 50 symbols!");
            if (pages < 0)
                throw new ArgumentOutOfRangeException("Pages cannot be negative!");
            if (country.Length < 3 || country.Length > 20)
                throw new ArgumentOutOfRangeException("Country should be between 3 and 20 symbols!");
            if (language.Length < 3 || language.Length > 20)
                throw new ArgumentOutOfRangeException("Language should be between 3 and 20 symbols!");
            if (category < 0 || category > (CategoryType)Enum.GetNames(typeof(CategoryType)).Length)
                throw new ArgumentException("No such category exists!");

            return new Book(title, authorId, pages, year, country, language, link, image, isbn, category);
        }
    }
}
