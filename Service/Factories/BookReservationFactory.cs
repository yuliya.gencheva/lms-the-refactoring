﻿
using LMS.Data.Models;
using LMS.Services.Contracts;

namespace LMS.Services.Factories
{
    public class BookReservationFactory : IBookReservationFactory
    {
        public BookReservation CreateBookReservation(BookItem bookToReserve, User user)
        {
            return new BookReservation(bookToReserve, user);
        }
    }
}
