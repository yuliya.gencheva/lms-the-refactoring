﻿
using LMS.Data.Models;
using LMS.Services.Contracts;

namespace LMS.Services.Factories
{
    public class BookItemFactory : IBookItemFactory
    {
        public BookItem CreateBookItem(Book book, int rack)
        {
            return new BookItem(book, rack);
        }
    }
}
