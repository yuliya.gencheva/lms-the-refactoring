﻿
using LMS.Data.Models;
using LMS.Services.Contracts;

namespace LMS.Services.Factories
{
    public class BookIssuedFactory : IBookIssuedFactory
    {
        public BookIssued CreateBookIssued(BookItem bookToIssue, User user)
        {
            return new BookIssued(bookToIssue, user);
        }
    }
}
