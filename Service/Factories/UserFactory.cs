﻿using LMS.Data.Models;
using LMS.Services.Contracts;
using System;

namespace LMS.Services.Factories
{
    public class UserFactory : IUserFactory
    {
        public User CreateUser(string name, string userName, int roleId)
        {
            if (name.Length < 3 || name.Length > 30)
                throw new ArgumentOutOfRangeException("Name should be between 3 and 30 symbols!");
            if (userName.Length < 3 || userName.Length > 30)
                throw new ArgumentOutOfRangeException("UserName should be between 3 and 30 symbols!");
            return new User(name, userName, roleId);
        }
    }
}
