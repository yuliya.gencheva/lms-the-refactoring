﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Data;
using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services.Contracts;
using Microsoft.EntityFrameworkCore;

namespace LMS.Services
{
    public class BookItemManager : IBookItemManager
    {
        private readonly LMSContext context;
        private readonly IBookManager bookManager;
        private readonly IBookItemFactory bookItemFactory;

        public BookItemManager(LMSContext context, IBookManager bookManager, IBookItemFactory bookItemFactory)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.bookManager = bookManager ?? throw new ArgumentNullException(nameof(bookManager));
            this.bookItemFactory = bookItemFactory ?? throw new ArgumentNullException(nameof(bookItemFactory));
        }

        public async Task AddBookItem(int? id, int rack)
        {
            var bookToAdd = await bookManager.GetBook(id);
            BookItem newBookItem = bookItemFactory.CreateBookItem(bookToAdd, rack);
            context.BookItems.Add(newBookItem);
            await context.SaveChangesAsync();
        }

        public BookItem GetBookItem(int id)
        {
            var bookItem = context.BookItems.Find(id);
            bookItem.ValidateIfNull($"No book item with id {id} exists in Database!");
            return bookItem;
        }

        public List<BookItem> GetBookItemsFromBook(Book book)
        {
            var bookItems = context.BookItems.Include(b => b.Book).Where(b => b.Book == book).ToList();
            bookItems.ValidateIfNull($"No book items for book with id {book.BookId} exist in the Database!");
            return bookItems;
        }

        public void RemoveBookItem(int bookItemId)
        {
            BookItem bookItemToRemove = GetBookItem(bookItemId);
            if (bookItemToRemove.BookStatus != BookStatus.Available)
            {
                throw new ArgumentException("The bookitem you are trying to remove is currently not available!");
            }
            context.BookItems.Remove(bookItemToRemove);
            context.SaveChanges();
        }


        public void ModifyBookItem(int id, string criteria, string newParameter)
        {
            BookItem booktemToModify = GetBookItem(id);

            switch (criteria.ToLower())
            {
                case "racknumber":
                    booktemToModify.ChangeRack(int.Parse(newParameter));
                    break;
                case "referenceonly":
                    booktemToModify.ReferenceOnly = bool.Parse(newParameter);
                    break;
                case "bookstatus":
                    booktemToModify.BookStatus = (BookStatus)Enum.Parse(typeof(BookStatus), newParameter,true);
                    break;
                default:
                    throw new ArgumentException("Invalid Command");
            }

            context.SaveChanges();
        }
    }
}
