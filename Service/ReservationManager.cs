﻿using LMS.Data;
using LMS.Data.Common;
using LMS.Data.Models;
using LMS.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMS.Services
{
    public class ReservationManager : IReservationManager
    {
        private readonly LMSContext context;
        private readonly IBookItemManager bookItemManager;
        private readonly ILibraryUserManager userManager;
        private readonly IBookReservationFactory bookReservationFactory;


        public ReservationManager(LMSContext context, IBookItemManager bookItemManager, ILibraryUserManager userManager,
            IBookReservationFactory bookReservationFactory)
        {
            this.context = context;
            this.bookItemManager = bookItemManager;
            this.userManager = userManager;
            this.bookReservationFactory = bookReservationFactory;
        }

        public BookReservation GetReservation(int bookItemId)
        {
            var bookReservation = context.BookReservations.Include(br => br.User).FirstOrDefault(br => br.BookItemId == bookItemId);
            bookReservation.ValidateIfNull($"BookItem with Id {bookItemId} is not reserved!");
            return bookReservation;
        }
        public int GetNumberOfReservationsPerUser(User user)
        {
            user.ValidateIfNull("Invalid user!");

            return context.BookReservations.Where(b => b.User.UserName == user.UserName).Count();
        }

        public void RemoveReservation(BookReservation reservation)
        {
            reservation.ValidateIfNull("Invalid reservation!");

            context.BookReservations.Remove(reservation);
        }

        public void ReserveBook(int bookItemId, string userName)
        {
            BookItem bookToReserve = bookItemManager.GetBookItem(bookItemId);
            //User userToReserveBook = userManager.GetUser(userName);
            if (bookToReserve.ReferenceOnly)
            {
                throw new ArgumentException("Book is reference only!");
            }
            //if (userToReserveBook.RoleId == 1)
            //{
            //    throw new Exception("Libararians cannot check-out books");
            //}


            //if (GetNumberOfReservationsPerUser(userToReserveBook) >= 5)
            //{
            //    throw new ArgumentException("Exceeded maximum number of books!");
            //}
            if (bookToReserve.BookStatus == BookStatus.Available)
            {
                this.bookItemManager.ModifyBookItem(bookToReserve.BookItemId, "bookstatus", "reserved");
            }
            //BookReservation reservation = bookReservationFactory.CreateBookReservation(bookToReserve, userToReserveBook);
            //context.BookReservations.Add(reservation);
            //context.SaveChanges();
        }

    }
}
