﻿using LMS.Data;
using LMS.Data.Models;
using LMS.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Services
{
    public class MembershipManager :IMembershipManager
    {
        private readonly LMSContext context;

        public MembershipManager(LMSContext context)
        {
            this.context = context;
        }

        public async Task<Membership> AddMembership(string userId, int term)
        {
            DateTime finalDate = DateTime.Now.AddMonths(term);
            var newMembership = new Membership() { UserId = userId, Validity = finalDate };
            await this.context.Memberships.AddAsync(newMembership);
            await this.context.SaveChangesAsync();
            return newMembership;
        }



    }
}
