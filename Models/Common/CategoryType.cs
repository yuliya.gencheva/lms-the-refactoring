﻿namespace LMS.Data.Common
{
    public enum CategoryType
    {
        Fantasy = 0,
        Science = 1,
        Western = 2,
        Romance = 3,
        Thriller = 4,
        Mystery = 5,
        Detective = 6,
        Piracy = 7,
        Horror = 8,
        Comics = 9
    }
}
