﻿namespace LMS.Data.Common
{
    public enum BookStatus
    {
        Reserved = 0,
        Loaned = 1,
        Available = 2
    }
}
