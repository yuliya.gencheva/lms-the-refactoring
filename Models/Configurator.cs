﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LMS.Data
{
    public class Configurator
    {
        private const string ConnectionStringAddress =
            @"../../../../Models/Configurations/Configuration.txt";
        public static string Configure()
        {
            return File.ReadAllText(ConnectionStringAddress);
        }

        public string ConfigurationString { get; private set; }
    }
}
