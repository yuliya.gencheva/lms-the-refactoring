﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace LMS.Data.Seed.IOSeed
{
    public class WriteFileDatabase : IWriteFileDatabase
    {
        public WriteFileDatabase()
        {

        }
        public void WriteJsonFile<T>(string address, IEnumerable<T> list)
        {
            var jsonToOutput = JsonConvert.SerializeObject(list, Formatting.Indented, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
            File.WriteAllText(address, jsonToOutput);
        }
    }
}
