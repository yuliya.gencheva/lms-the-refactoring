﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace LMS.Data.Seed.IOSeed
{
    public class ReadFileDatabase
    {
        public ReadFileDatabase()
        {

        }
        public static List<T> ReadJsonsSeeds<T>(string address)
        {
            var json = File.ReadAllText(address);
            var settings = new JsonSerializerSettings();
            var lists = JsonConvert.DeserializeObject<List<T>>(json, settings);
            return lists;
        }
    }
}
