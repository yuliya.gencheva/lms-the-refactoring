﻿using System.Collections.Generic;

namespace LMS.Data.Seed.IOSeed
{
    public interface IWriteFileDatabase
    {
        void WriteJsonFile<T>(string jsonAddress, IEnumerable<T> list);
    }
}
