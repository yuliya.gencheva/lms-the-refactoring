﻿//using LMS.Data;
//using LMS.Data.Models;
//using LMS.Data.Seed.IOSeed;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace LMS.Data.Seed
//{
//    public class DataSeeder
//    {
//        public static void SeedRoles(LMSContext context)
//        {
//            if (context.Roles.Any())
//                return;
//            context.Roles.AddRange(
//                new List<Role>
//                {
//                    new Role{RoleId=1, Name="Librarian"},
//                    new Role{RoleId=2, Name="Member"},
//                });

//            context.SaveChanges();
//        }

//        public static void SeedAuthors(LMSContext context)
//        {
//            const string authorsJsonAddress = @"../../../../Models/Seed/JsonFilesSeed/Authors.json";

//            if (context.Authors.Any())
//                return;

//            var listAuthors = ReadFileDatabase.ReadJsonsSeeds<Author>(authorsJsonAddress);

//            context.Authors.AddRange(
//                listAuthors.Select(author => new Author
//                {
//                    Name = author.Name
//                }));

//            context.SaveChanges();
//        }
//        public static void SeedBooks(LMSContext context)
//        {
//            const string booksJsonAddress = @"../../../../Models/Seed/JsonFilesSeed/Books.json";

//            if (context.Books.Any())
//                return;

//            var listBooks = ReadFileDatabase.ReadJsonsSeeds<Book>(booksJsonAddress);

//            foreach (var book in listBooks)
//            {
//                var author = context.Authors.SingleOrDefault(a => a.Name == book.Author.Name);

//                if (author == null)
//                {
//                    author = new Author { Name = book.Author.Name };
//                    context.Authors.Add(author);
//                    context.SaveChanges();
//                }

//                context.Books.Add(new Book
//                {
//                    Title = book.Title,
//                    AuthorId = author.AuthorId,
//                    Pages = book.Pages,
//                    Year = book.Year,
//                    Country = book.Country,
//                    Language = book.Language,
//                    Link = book.Link,
//                    ISBN = book.ISBN,
//                    Category = book.Category
//                });

//                context.SaveChanges();
//            }
//        }

//        public static void SeedBookItems(LMSContext context)
//        {
//            const string bookItemJsonAddress = @"../../../../Models/Seed/JsonFilesSeed/BooksItems.json";

//            if (context.BookItems.Any())
//                return;

//            var listBookItems = ReadFileDatabase.ReadJsonsSeeds<BookItem>(bookItemJsonAddress);

//            foreach (var bookItem in listBookItems)
//            {
//                var book = context.Books.FirstOrDefault(b => b.Title == bookItem.Book.Title && b.Author.Name == bookItem.Book.Author.Name);

//                context.BookItems.Add(
//                    new BookItem
//                    {
//                        BookID = book.BookId,
//                        Book = book,
//                        ReferenceOnly = bookItem.ReferenceOnly,
//                        BookStatus = bookItem.BookStatus,
//                        RackNumber = bookItem.RackNumber
//                    });
//            }

//            context.SaveChanges();
//        }

//        public static void SeedUsers(LMSContext context)
//        {
//            const string userJsonAddress = @"../../../../Models/Seed/JsonFilesSeed/Users.json";

//            if (context.Users.Any())
//                return;

//            var listUsers = ReadFileDatabase.ReadJsonsSeeds<User>(userJsonAddress);

//            context.Users.AddRange(
//                listUsers.Select(user => new User
//                {
//                    Name = user.Name,
//                    UserName = user.UserName,
//                    RoleId = user.RoleId
//                }));

//            context.SaveChanges();
//        }

//        public static void SeedBooksIssued(LMSContext context)
//        {
//            const string bookIssuedJsonAddress = @"../../../../Models/Seed/JsonFilesSeed/BooksIssued.json";

//            if (context.BooksIssued.Any())
//                return;

//            var listBookIssued = ReadFileDatabase.ReadJsonsSeeds<BookIssued>(bookIssuedJsonAddress);

//            context.BooksIssued.AddRange(
//                listBookIssued.Select(bookIssued => new BookIssued
//                {
//                    BookItemId = bookIssued.BookItemId,
//                    UserId = bookIssued.UserId,
//                    DateOfIssue = bookIssued.DateOfIssue
//                }));

//            context.SaveChanges();
//        }

//        public static void SeedBookReservations(LMSContext context)
//        {
//            const string bookReservationJsonAddress = @"../../../../Models/Seed/JsonFilesSeed/BookReservations.json";

//            if (context.BookReservations.Any())
//                return;

//            var listBookReservered = ReadFileDatabase.ReadJsonsSeeds<BookReservation>(bookReservationJsonAddress);

//            context.BookReservations.AddRange(
//                listBookReservered.Select(bookReservered => new BookReservation
//                {
//                    BookItemId = bookReservered.BookItemId,
//                    //BookItem= bookReservered.BookItem,
//                    UserId = bookReservered.UserId,
//                    //User = bookReservered.User

//                }));

//            context.SaveChanges();
//        }
//    }
//}
