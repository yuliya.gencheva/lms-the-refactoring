﻿using LMS.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LMS.Data.Configurations
{
    internal class BookIssuedConfiguration : IEntityTypeConfiguration<BookIssued>
    {
        public void Configure(EntityTypeBuilder<BookIssued> builder)
        {
            builder.HasKey(bi => bi.BookIssuedId);

            builder
                .Property(bi => bi.DateOfIssue)
                .IsRequired();

            builder
               .HasOne(bi => bi.User)
               .WithMany(user => user.BooksIssued)
               .HasForeignKey(bi => bi.UserId);
        }
    }
}