﻿using LMS.Data.Common;
using LMS.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace LMS.Data.Configurations
{
    internal class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder
                .HasKey(book => book.BookId);

            builder
                .Property(book => book.Title)
                .IsRequired();

            builder
                .Property(book => book.Year)
                .IsRequired();

            builder
                .Property(book => book.Category)
                .IsRequired()
                .HasConversion(b => b.ToString(), b => (CategoryType)Enum.Parse(typeof(CategoryType), b));
        }
    }
}