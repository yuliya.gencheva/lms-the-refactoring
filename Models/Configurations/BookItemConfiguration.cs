﻿using LMS.Data.Common;
using LMS.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LMS.Data.Configurations
{
    internal class BookItemConfiguration : IEntityTypeConfiguration<BookItem>
    {
        public void Configure(EntityTypeBuilder<BookItem> builder)
        {
            builder.HasKey(bookItem => bookItem.BookItemId);
            builder
                .HasOne(bookItem => bookItem.Book)
                .WithMany(book => book.BookItems)
                .HasForeignKey(bookItem => bookItem.BookID);

            builder
                .Property(bookItem => bookItem.BookStatus)
                .HasDefaultValue(BookStatus.Available)
                .IsRequired();
            builder
                .Property(bookItem => bookItem.RackNumber)
                .IsRequired();
            builder
                .Property(bookItem => bookItem.ReferenceOnly)
                .HasDefaultValue(false)
                .IsRequired();



        }
    }
}