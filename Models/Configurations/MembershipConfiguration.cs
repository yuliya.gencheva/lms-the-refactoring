﻿using LMS.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Configurations
{
    class MembershipConfiguration : IEntityTypeConfiguration<Membership>
    {
        void IEntityTypeConfiguration<Membership>.Configure(EntityTypeBuilder<Membership> builder)
        {
            builder
               .HasKey(mem => mem.Id);

            builder
               .HasOne(m => m.User)
               .WithOne(user => user.Membership)
               .HasForeignKey<Membership>(m => m.UserId);
               
        }
    }
}
