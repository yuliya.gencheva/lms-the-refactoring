﻿using LMS.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Configurations
{
    public class AuthorConfiguration : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder
                .HasKey(author => author.AuthorId);

            builder
                .Property(author => author.Name)
                .HasMaxLength(35)
                .IsRequired();


            builder
                .HasMany(b => b.Books)
                .WithOne(b => b.Author)
                .HasForeignKey(b => b.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);


            builder
            .HasIndex(author => author.Name)
            .IsUnique();
        }
    }
}
