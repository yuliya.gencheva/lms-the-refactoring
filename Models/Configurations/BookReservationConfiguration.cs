﻿using LMS.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LMS.Data.Configurations
{
    internal class BookReservationConfiguration : IEntityTypeConfiguration<BookReservation>
    {
        public void Configure(EntityTypeBuilder<BookReservation> builder)
        {
            builder.HasKey(br => new { br.BookItemId, br.UserId });

            builder
                .HasOne(br => br.BookItem)
                .WithMany(bookItem => bookItem.BookReservations)
                .HasForeignKey(br => br.BookItemId);

            builder
               .HasOne(br => br.User)
               .WithMany(user => user.BookReservations)
               .HasForeignKey(br => br.UserId);
        }
    }
}