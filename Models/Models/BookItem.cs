﻿using LMS.Data.Common;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Models
{
    public class BookItem
    {

        public BookItem()
        {
            BookReservations = new List<BookReservation>();
        }
        public BookItem(Book book, int rackNumber)
        {
            Book = book;
            RackNumber = rackNumber;
            BookReservations = new List<BookReservation>();
        }

        //Properties
        #region
        public int BookItemId { get; set; }
        public bool ReferenceOnly { get; set; }
        public BookStatus BookStatus { get; set; }
        public int RackNumber { get; set; }
        public int BookID { get; set; }
        public Book Book { get; set; }
        public BookIssued BookIssued { get; set; }
        public ICollection<BookReservation> BookReservations { get; set; }

        public bool IsDeleted { get; set; } = false;
        #endregion

        public void ChangeBookStatus(BookStatus bookStatus)
        {
            BookStatus = bookStatus;
        }
        public void ChangeRack(int newRack)
        {

            RackNumber = newRack;
        }
        //Polymorphism
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Bookitem id:{BookItemId}");
            sb.AppendLine(Book.ToString());
            sb.AppendLine($"Current status: {BookStatus}, Rack: {RackNumber}");

            return sb.ToString().Trim();
        }
    }
}
