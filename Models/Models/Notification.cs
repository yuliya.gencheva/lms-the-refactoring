﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Models
{
    public class Notification
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsSeen { get; set; } = false;
        public string UserId { get; set; }
        public User Users { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
