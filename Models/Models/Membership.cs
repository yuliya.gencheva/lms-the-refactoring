﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Models
{
    public class Membership
    {
        public int Id { get; set; }
        public DateTime Created { get; set; } = DateTime.Now.Date;
        public DateTime Validity { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
    }
}
