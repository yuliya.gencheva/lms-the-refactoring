﻿using LMS.Data.Common;
using System;

namespace LMS.Data.Models
{
    public class BookIssued
    {
        public BookIssued()
        {

        }
        public BookIssued(BookItem bookItem, User user)
        {
            bookItem.ValidateIfNull("You cannot create a new bookIssued with null bookItem");
            user.ValidateIfNull("You cannot create a new bookIssued with null user");
            BookItem = bookItem;
            User = user;
            DateOfIssue = DateTime.Now.Date;
        }

        public int BookIssuedId { get; set; }
        public DateTime DateOfIssue { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public int BookItemId { get; set; }
        public BookItem BookItem { get; set; }

        public bool IsDeleted { get; set; } = false;

    }
}
