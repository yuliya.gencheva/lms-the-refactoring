﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Models
{
    public class Author
    {
        public Author()
        {
            Books = new List<Book>();
        }

        public Author(string name)
        {
            Name = name;
            Books = new List<Book>();
        }

        public int AuthorId { get; set; }
        public string Name { get; set; }
        public List<Book> Books { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
