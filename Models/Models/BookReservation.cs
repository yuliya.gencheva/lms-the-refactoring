﻿using LMS.Data.Common;

namespace LMS.Data.Models
{
    public class BookReservation //: IBookReservation
    {

        public BookReservation()
        {

        }
        public BookReservation(BookItem bookItem, User user)
        {
            bookItem.ValidateIfNull("You cannot create a new reservation with null bookItem");
            user.ValidateIfNull("You cannot create a new reservation with null user");
            BookItem = bookItem;
            User = user;
        }

        public int BookItemId { get; set; }
        public BookItem BookItem { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
