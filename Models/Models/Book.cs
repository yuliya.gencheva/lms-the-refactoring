﻿using LMS.Data.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Models
{
    public class Book
    {
        public Book()
        {
            BookItems = new List<BookItem>();
            Review = new List<Review>();
        }
        public Book(
            string title, 
            int authorId, 
            int pages, 
            int year, 
            string country, 
            string language, 
            string link, 
            string image, 
            string isbn, 
            CategoryType category)
        {
            Title = title;
            AuthorId = authorId;
            Pages = pages;
            Year = year;
            Country = country;
            Language = language;
            Link = link;
            ImageURL = image;
            ISBN = isbn;
            Category = category;
            BookItems = new List<BookItem>();
            Review = new List<Review>();
        }

        //Properties
        #region

        public int BookId { get; set; }

        public string Title { get; set; }

        public int AuthorId { get; set; }

        public Author Author { get; set; }

        public int Pages { get; set; }

        public int Year { get; set; }

        public string Country { get; set; }

        public string Language { get; set; }

        public string Link { get; set; }

        public string ImageURL { get; set; }

        public string ISBN { get; set; }

        public CategoryType Category { get; set; }

        public ICollection<Review> Review { get; set; }

        public ICollection<BookItem> BookItems { get; set; }

        public bool IsDeleted { get; set; } = false;
        #endregion

        public override string ToString()
        {
            var sb = new StringBuilder($"title: {Title} " + Environment.NewLine
                + $"author: {Author.Name} " + Environment.NewLine +
                $"pages: {Pages} " + Environment.NewLine +
                $"year: {Year} " + Environment.NewLine +
                $"country: {Country} " + Environment.NewLine +
                $"language: {Language} " + Environment.NewLine +
                $"url: {Link}");
            return $"{sb.ToString()}";
        }

        //Open/Closed Principle
        public override bool Equals(object book)
        {
            var bookToCompare = book as Book;
            bool isFound = false;
            if (bookToCompare.Title == Title && bookToCompare.Author.Name == Author.Name && bookToCompare.Year == Year)
                isFound = true;
            return isFound;
        }

        public override int GetHashCode()
        {
            return new { Title, Author, Year }.GetHashCode();
        }
    }
}

