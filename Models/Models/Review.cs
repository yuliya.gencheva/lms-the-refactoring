﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Models
{
    public class Review
    {
        public int Id { get; set; }
        public double Rating { get; set; }
        public string TextReview { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
