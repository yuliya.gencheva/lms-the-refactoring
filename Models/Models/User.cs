﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LMS.Data.Models
{
    public class User:IdentityUser
    {
        public User()
        {
            BookReservations = new List<BookReservation>();
            BooksIssued = new List<BookIssued>();
            Notifications = new List<Notification>();
        }

        public User(string name, string UserName, int roleId)
        {
            BookReservations = new List<BookReservation>();
            BooksIssued = new List<BookIssued>();
            Notifications = new List<Notification>();
        }

        public ICollection<BookReservation> BookReservations { get; set; }
        public ICollection<BookIssued> BooksIssued { get; set; }
        public bool IsBanned { get; set; }
        public ICollection<Notification> Notifications { get; set; }

        public bool IsDeleted { get; set; } = false;

        public int? MembershipId { get; set; }
        public Membership Membership { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Userame:{base.UserName}");
            sb.AppendLine($"Role: {base.Email}");
            return sb.ToString().Trim();
        }
    }
}

//public ICollection<BookIssued> CheckOutBooks { get; set; }

//public ICollection<BookReservation> ReservedBooks { get; set; }

//public List<string> Notifications
//{
//    get
//    {
//        return new List<String> (this.notifications );
//    }
//}

//public void AddNotification(string notification)
//{
//    notification.ValidateIfNull();
//    this.notifications.Add(notification);
//}